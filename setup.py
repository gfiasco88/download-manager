#!/usr/bin/env python

from setuptools import setup, find_packages

setup(name='download-manager',
      version='0.0.1c',
      description='python download manager',
      author='Gian Luca Fiasco',
      author_email='glf@lucacloud.info',
      packages=find_packages(),
      url='https://gitlab.lucacloud.info/fiasco/download-manager.git',
      license='MIT',
      )