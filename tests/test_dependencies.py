#!/usr/bin/env python3
import importlib
import os
libdir='../manager/'
gbl = globals()
error=False
for f in os.listdir(libdir):
    if f != '__init__.py' and f.endswith('.py'):
        moduleToImport = libdir+f.split('.py')[0]
        try:
            gbl[moduleToImport] = importlib.import_module(moduleToImport)
        except Exception as e:
            print (f,e)
            error=True

if error:
    raise Exception('Test is not passed')
