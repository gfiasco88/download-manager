import os

adminUser=os.getenv('flask_user',default='admin')
adminPass=os.getenv('flask_passwd',default='password')
downloadPath='/download'
postProcessPath='/music'
numWorkers=int(os.getenv('num_workers',default=2))
ytfilters={
    'only_audio':True,
    }
spToken=os.getenv('sptoken',default='None')
fmApiKey=os.getenv('fmapikey',default='f4aa1cac32f135ec8257894e4108ce33')