
// Copyright 2012 Google Inc. All rights reserved.
(function(w,g){w[g]=w[g]||{};w[g].e=function(s){return eval(s);};})(window,'google_tag_manager');(function(){

var data = {
"resource": {
  "version":"348",
  "macros":[{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){return Math.floor(2*Math.random())})();"]
    },{
      "function":"__v",
      "vtp_name":"CustomerId",
      "vtp_defaultValue":"",
      "vtp_setDefaultValue":true,
      "vtp_dataLayerVersion":2
    },{
      "function":"__k",
      "vtp_decodeCookie":false,
      "vtp_name":"customer_id_cookie"
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){return\/(,|^)",["escape",["macro",1],9],"\/.test(",["escape",["macro",2],8,16],")})();"]
    },{
      "function":"__k",
      "vtp_decodeCookie":false,
      "vtp_name":"free_trial_customer_id_cookie"
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){return\/(,|^)",["escape",["macro",1],9],"\/.test(",["escape",["macro",4],8,16],")})();"]
    },{
      "function":"__v",
      "vtp_name":"MinutesSinceCreation",
      "vtp_defaultValue":"-1",
      "vtp_setDefaultValue":true,
      "vtp_dataLayerVersion":2
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){var b=\"\";try{var a=String(",["escape",["macro",6],8,16],");a=a.replace(\",\",\".\");b=Math.floor(a)}catch(c){}return b})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){var q=\"UA-48375203-3\",z=\"auto\",F=null;!function(){function h(a){Ta.set(a)}function q(a){}function r(){}function z(){}function F(a){}function Ua(a){}function Va(a){}function t(a,b,c,e){b[a]=function(){try{return e\u0026\u0026h(e),c.apply(this,arguments)}catch(v){throw v;}}}function ca(a,b,c){\"none\"==b\u0026\u0026(b=\"\");var e=[],v=G(a);a=\"__utma\"==a?6:2;for(var d=0;d\u003Cv.length;d++){var g=(\"\"+v[d]).split(\".\");g.length\u003E=a\u0026\u0026e.push({hash:g[0],R:v[d],O:g})}if(0!=e.length)return 1==e.length?e[0]:da(b,e)||da(c,e)||\nda(null,e)||e[0]}function da(a,b){var c;null==a?c=a=1:(c=L(a),a=L(0==a.indexOf(\".\")?a.substring(1):\".\"+a));for(var e=0;e\u003Cb.length;e++)if(b[e].hash==c||b[e].hash==a)return b[e]}function Wa(a){var b=a.get(w);if(a.get(A))return a=a.get(H),c=C(a+b,0),\"_ga\\x3d2.\"+M(c+\".\"+a+\"-\"+b);var c=C(b,0);return\"_ga\\x3d1.\"+M(c+\".\"+b)}function C(a,b){var c=new Date,e=p.navigator,d=e.plugins||[];a=[a,e.userAgent,c.getTimezoneOffset(),c.getYear(),c.getDate(),c.getHours(),c.getMinutes()+b];for(b=0;b\u003Cd.length;++b)a.push(d[b].description);\nreturn L(a.join(\".\"))}function sa(a,b){if(b==k.location.hostname)return!1;for(var c=0;c\u003Ca.length;c++)if(a[c]instanceof RegExp){if(a[c].test(b))return!0}else if(0\u003C=b.indexOf(a[c]))return!0;return!1}function L(a){var b,c=1;if(a)for(c=0,b=a.length-1;0\u003C=b;b--){var e=a.charCodeAt(b);c=(c\u003C\u003C6\u0026268435455)+e+(e\u003C\u003C14);e=266338304\u0026c;c=0!=e?c^e\u003E\u003E21:c}return c}var Q=function(a){this.w=a||[]};Q.prototype.set=function(a){this.w[a]=!0};Q.prototype.encode=function(){for(var a=[],b=0;b\u003Cthis.w.length;b++)this.w[b]\u0026\u0026(a[Math.floor(b\/\n6)]^=1\u003C\u003Cb%6);for(b=0;b\u003Ca.length;b++)a[b]=\"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_\".charAt(a[b]||0);return a.join(\"\")+\"~\"};var Ta=new Q,Xa=function(a){return a?a.replace(\/^[\\s\\xa0]+|[\\s\\xa0]+$\/g,\"\"):\"\"},ta=function(){for(var a=p.navigator.userAgent+(k.cookie?k.cookie:\"\")+(k.referrer?k.referrer:\"\"),b=a.length,c=p.history.length;0\u003Cc;)a+=c--^b++;return[R()^2147483647\u0026L(a),Math.round((new Date).getTime()\/1E3)].join(\".\")},Ya=function(){},M=function(a){return encodeURIComponent instanceof\nFunction?encodeURIComponent(a):(h(28),a)},S=function(a,b,c,e){try{a.addEventListener?a.addEventListener(b,c,!!e):a.attachEvent\u0026\u0026a.attachEvent(\"on\"+b,c)}catch(v){h(27)}},ua=\/^[\\w\\-:\/.?=\u0026%!]+$\/,T=function(){var a=\"\"+k.location.hostname;return 0==a.indexOf(\"www.\")?a.substring(4):a},va=function(a,b){if(1==b.length\u0026\u0026null!=b[0]\u0026\u0026\"object\"==typeof b[0])return b[0];for(var c={},e=Math.min(a.length+1,b.length),d=0;d\u003Ce;d++){if(\"object\"==typeof b[d]){for(var f in b[d])b[d].hasOwnProperty(f)\u0026\u0026(c[f]=b[d][f]);break}d\u003C\na.length\u0026\u0026(c[a[d]]=b[d])}return c},D=function(){this.keys=[];this.values={};this.m={}};D.prototype.set=function(a,b,c){this.keys.push(a);c?this.m[\":\"+a]=b:this.values[\":\"+a]=b};D.prototype.get=function(a){return this.m.hasOwnProperty(\":\"+a)?this.m[\":\"+a]:this.values[\":\"+a]};D.prototype.map=function(a){for(var b=0;b\u003Cthis.keys.length;b++){var c=this.keys[b],e=this.get(c);e\u0026\u0026a(c,e)}};var p=window,k=document,ea=window,G=function(a){var b=[],c=k.cookie.split(\";\");a=new RegExp(\"^\\\\s*\"+a+\"\\x3d\\\\s*(.*?)\\\\s*$\");\nfor(var e=0;e\u003Cc.length;e++){var d=c[e].match(a);d\u0026\u0026b.push(d[1])}return b},U=function(a,b,c,e,d,f){a:{var v=ea._gaUserPrefs;if(v\u0026\u0026v.ioo\u0026\u0026v.ioo()||d\u0026\u0026!0===ea[\"ga-disable-\"+d])var g=!0;else{try{var l=ea.external;if(l\u0026\u0026l._gaUserPrefs\u0026\u0026\"oo\"==l._gaUserPrefs){g=!0;break a}}catch(Vb){}g=!1}}if(g||Za.test(k.location.hostname)||\"\/\"==c\u0026\u0026$a.test(e))return!1;if(b\u0026\u00261200\u003Cb.length\u0026\u0026(b=b.substring(0,1200),h(24)),c=a+\"\\x3d\"+b+\"; path\\x3d\"+c+\"; \",f\u0026\u0026(c+=\"expires\\x3d\"+(new Date((new Date).getTime()+f)).toGMTString()+\n\"; \"),e\u0026\u0026\"none\"!=e\u0026\u0026(c+=\"domain\\x3d\"+e+\";\"),e=k.cookie,k.cookie=c,!(e=e!=k.cookie))a:{a=G(a);for(e=0;e\u003Ca.length;e++)if(b==a[e]){e=!0;break a}e=!1}return e},fa=function(a){return M(a).replace(\/\\(\/g,\"%28\").replace(\/\\)\/g,\"%29\")},$a=\/^(www\\.)?google(\\.com?)?(\\.[a-z]{2})?$\/,Za=\/(^|\\.)doubleclick\\.net$\/i,wa=function(){this.M=[]};wa.prototype.add=function(a){};var R=function(){return Math.round(2147483647*Math.random())},V=function(){this.data=new D},W=new D,ha=[];V.prototype.get=function(a){var b=xa(a),\nc=this.data.get(a);return b\u0026\u0026void 0==c\u0026\u0026(c=\"function\"==typeof b.defaultValue?b.defaultValue():b.defaultValue),b\u0026\u0026b.Z?b.Z(this,a,c):c};var m=function(a,b){return a=a.get(b),void 0==a?\"\":\"\"+a},ia=function(a,b){return a=a.get(b),void 0==a||\"\"===a?0:1*a};V.prototype.set=function(a,b,c){if(a)if(\"object\"==typeof a)for(var e in a)a.hasOwnProperty(e)\u0026\u0026ya(this,e,a[e],c);else ya(this,a,b,c)};var ya=function(a,b,c,e){if(void 0!=c)switch(b){case B:ab.test(c)}var d=xa(b);d\u0026\u0026d.o?d.o(a,b,c,e):a.data.set(b,c,e)},\nN=function(a,b,c,e,d){this.name=a;this.F=b;this.Z=e;this.o=d;this.defaultValue=c},xa=function(a){var b=W.get(a);if(!b)for(var c=0;c\u003Cha.length;c++){var e=ha[c],d=e[0].exec(a);if(d){b=e[1](d);W.set(b.name,b);break}}return b},bb=function(a){var b;return W.map(function(c,e){e.F==a\u0026\u0026(b=e)}),b\u0026\u0026b.name},d=function(a,b,c,e,d){return a=new N(a,b,c,e,d),W.set(a.name,a),a.name},X=function(a,b){ha.push([new RegExp(\"^\"+a+\"$\"),b])},l=function(a,b,c){return d(a,b,c,void 0,za)},za=function(){},O=\"slga\",Y=!1,cb=l(\"apiVersion\",\n\"v\"),db=l(\"clientVersion\",\"_v\");d(\"anonymizeIp\",\"aip\");var eb=d(\"adSenseId\",\"a\"),Aa=d(\"hitType\",\"t\");d(\"hitCallback\");d(\"hitPayload\");d(\"nonInteraction\",\"ni\");d(\"currencyCode\",\"cu\");d(\"dataSource\",\"ds\");d(\"useBeacon\",void 0,!1);d(\"transport\");d(\"sessionControl\",\"sc\",\"\");d(\"sessionGroup\",\"sg\");d(\"queueTime\",\"qt\");d(\"_s\",\"_s\");d(\"screenName\",\"cd\");var fb=(d(\"location\",\"dl\",\"\"),d(\"referrer\",\"dr\"),d(\"page\",\"dp\",\"\"));d(\"hostname\",\"dh\");d(\"language\",\"ul\");d(\"encoding\",\"de\");d(\"title\",\"dt\",function(){return k.title||\nvoid 0});X(\"contentGroup([0-9]+)\",function(a){return new N(a[0],\"cg\"+a[1])});d(\"screenColors\",\"sd\");d(\"screenResolution\",\"sr\");d(\"viewportSize\",\"vp\");d(\"javaEnabled\",\"je\");d(\"flashVersion\",\"fl\");d(\"campaignId\",\"ci\");d(\"campaignName\",\"cn\");d(\"campaignSource\",\"cs\");d(\"campaignMedium\",\"cm\");d(\"campaignKeyword\",\"ck\");d(\"campaignContent\",\"cc\");var gb=d(\"eventCategory\",\"ec\"),hb=d(\"eventAction\",\"ea\"),ib=d(\"eventLabel\",\"el\"),jb=d(\"eventValue\",\"ev\"),kb=d(\"socialNetwork\",\"sn\"),lb=d(\"socialAction\",\"sa\"),mb=\nd(\"socialTarget\",\"st\"),nb=(d(\"l1\",\"plt\"),d(\"l2\",\"pdt\"),d(\"l3\",\"dns\"),d(\"l4\",\"rrt\"),d(\"l5\",\"srt\"),d(\"l6\",\"tcp\"),d(\"l7\",\"dit\"),d(\"l8\",\"clt\"),d(\"timingCategory\",\"utc\")),ob=d(\"timingVar\",\"utv\"),pb=d(\"timingLabel\",\"utl\"),qb=d(\"timingValue\",\"utt\");d(\"appName\",\"an\");d(\"appVersion\",\"av\",\"\");d(\"appId\",\"aid\",\"\");d(\"appInstallerId\",\"aiid\",\"\");d(\"exDescription\",\"exd\");d(\"exFatal\",\"exf\");var rb=(d(\"expId\",\"xid\"),d(\"expVar\",\"xvar\"),d(\"exp\",\"exp\"),d(\"_utma\",\"_utma\")),sb=d(\"_utmz\",\"_utmz\"),tb=d(\"_utmht\",\"_utmht\");\nd(\"_hc\",void 0,0);d(\"_ti\",void 0,0);d(\"_to\",void 0,20);X(\"dimension([0-9]+)\",function(a){return new N(a[0],\"cd\"+a[1])});X(\"metric([0-9]+)\",function(a){return new N(a[0],\"cm\"+a[1])});d(\"linkerParam\",void 0,void 0,Wa,za);d(\"usage\",\"_u\");var Ba=d(\"_um\");d(\"forceSSL\",void 0,void 0,function(){return Y},function(a,b,c){h(34);Y=!!c});var ub=d(\"_j1\",\"jid\"),vb=d(\"_j2\",\"gjid\");X(\"\\\\\\x26(.*)\",function(a){var b=new N(a[0],a[1]),c=bb(a[0].substring(1));return c\u0026\u0026(b.Z=function(a){return a.get(c)},b.o=function(a,\nb,d,g){a.set(c,d,g)},b.F=void 0),b});var wb=l(\"_oot\"),xb=d(\"previewTask\"),yb=d(\"checkProtocolTask\"),zb=d(\"validationTask\"),Ab=d(\"checkStorageTask\"),Bb=d(\"historyImportTask\"),Cb=(d(\"samplerTask\"),d(\"_rlt\"));d(\"buildHitTask\");d(\"sendHitTask\");var Db=(d(\"ceTask\"),d(\"devIdTask\")),Eb=(d(\"timingTask\"),d(\"displayFeaturesTask\")),y=l(\"name\"),w=l(\"clientId\",\"cid\"),Ca=l(\"clientIdTime\"),Da=d(\"userId\",\"uid\"),B=l(\"trackingId\",\"tid\"),P=l(\"cookieName\",void 0,\"_ga\"),u=l(\"cookieDomain\"),E=l(\"cookiePath\",void 0,\"\/\"),\nja=l(\"cookieExpires\",void 0,63072E3),Z=l(\"legacyCookieDomain\"),ka=l(\"legacyHistoryImport\",void 0,!0),I=l(\"storage\",void 0,\"cookie\"),la=l(\"allowLinker\",void 0,!1),ma=l(\"allowAnchor\",void 0,!0),Ea=l(\"sampleRate\",\"sf\",100),na=l(\"siteSpeedSampleRate\",void 0,1),Fa=l(\"alwaysSendReferrer\",void 0,!1),H=l(\"_gid\",\"_gid\"),A=l(\"_ge\"),oa=l(\"_gcn\"),Fb=d(\"transportUrl\"),Gb=d(\"_r\",\"_r\"),pa=function(a,b,c){this.V=a;this.fa=b;this.$=!1;this.oa=c;this.ea=1},qa=function(a,b,c){if(a.fa\u0026\u0026a.$)return 0;if(a.$=!0,b){if(a.oa\u0026\u0026\nia(b,a.oa))return ia(b,a.oa);if(0==b.get(na))return 0}return 0==a.V?0:(void 0===c\u0026\u0026(c=void 0),0==c%a.V?Math.floor(c\/a.V)%a.ea+1:0)},J=!1,Ha=function(a){\"cookie\"==m(a,I)\u0026\u0026(Ga(a,w,P),a.get(A)\u0026\u0026Ga(a,H,oa,864E5))},Ga=function(a,b,c,e){var d=Ia(a,b);if(d){c=m(a,c);b=Ja(m(a,E));var f=ra(m(a,u));e=e||1E3*ia(a,ja);var g=m(a,B);if(\"auto\"!=f)U(c,d,b,f,g,e)\u0026\u0026(J=!0);else{h(32);var x;if(d=[],f=T().split(\".\"),4!=f.length||(x=f[f.length-1],parseInt(x,10)!=x)){for(x=f.length-2;0\u003C=x;x--)d.push(f.slice(x).join(\".\"));\nd.push(\"none\");x=d}else x=[\"none\"];for(var k=0;k\u003Cx.length;k++)if(f=x[k],a.data.set(u,f),d=Ia(a,w),U(c,d,b,f,g,e))return void(J=!0);a.data.set(u,\"auto\")}}else a.get(A)||h(54)},Hb=function(a){if(\"cookie\"==m(a,I)\u0026\u0026!J\u0026\u0026(Ha(a),!J))throw\"abort\";},Ib=function(a){if(a.get(ka)){var b=m(a,u),c=m(a,Z)||T(),e=ca(\"__utma\",c,b);e\u0026\u0026(h(19),a.set(tb,(new Date).getTime(),!0),a.set(rb,e.R),(b=ca(\"__utmz\",c,b))\u0026\u0026e.hash==b.hash\u0026\u0026a.set(sb,b.R))}},Ia=function(a,b){b=fa(m(a,b));var c=ra(m(a,u)).split(\".\").length;return a=\nKa(m(a,E)),1\u003Ca\u0026\u0026(c+=\"-\"+a),b?[\"GA1\",c,b].join(\".\"):\"\"},Ma=function(a,b){if(b\u0026\u0026!(1\u003Eb.length)){for(var c=[],e=0;e\u003Cb.length;e++){var d=b[e].split(\".\");var f=d.shift();(\"GA1\"==f||\"1\"==f)\u0026\u00261\u003Cd.length?(f=d.shift().split(\"-\"),1==f.length\u0026\u0026(f[1]=\"1\"),f[0]*=1,f[1]*=1,d={H:f,s:d.join(\".\")}):d=void 0;d\u0026\u0026c.push(d)}if(1==c.length)return h(13),c[0].s;if(0!=c.length)return h(14),b=ra(m(a,u)).split(\".\").length,c=La(c,b,0),1==c.length?c[0].s:(a=Ka(m(a,E)),c=La(c,a,1),c[0]\u0026\u0026c[0].s);h(12)}},La=function(a,b,c){for(var e,\nd=[],f=[],g=0;g\u003Ca.length;g++){var h=a[g];h.H[c]==b?d.push(h):void 0==e||h.H[c]\u003Ce?(f=[h],e=h.H[c]):h.H[c]==e\u0026\u0026f.push(h)}return 0\u003Cd.length?d:f},ra=function(a){return 0==a.indexOf(\".\")?a.substr(1):a},Ja=function(a){return a?(1\u003Ca.length\u0026\u0026a.lastIndexOf(\"\/\")==a.length-1\u0026\u0026(a=a.substr(0,a.length-1)),0!=a.indexOf(\"\/\")\u0026\u0026(a=\"\/\"+a),a):\"\/\"},Ka=function(a){return a=Ja(a),\"\/\"==a?1:a.split(\"\/\").length},Jb=new RegExp(\/^https?:\\\/\\\/([^\\\/:]+)\/),Kb=\/(.*)([?\u0026#])(?:_ga=[^\u0026#]*)(?:\u0026?)(.*)\/,aa=function(a){h(48);this.target=\na;this.T=!1};aa.prototype.ca=function(a,b){if(a.tagName){if(\"a\"==a.tagName.toLowerCase())return void(a.href\u0026\u0026(a.href=ba(this,a.href,b)));if(\"form\"==a.tagName.toLowerCase())return Na(this,a)}if(\"string\"==typeof a)return ba(this,a,b)};var ba=function(a,b,c){var e=Kb.exec(b);e\u0026\u00263\u003C=e.length\u0026\u0026(b=e[1]+(e[3]?e[2]+e[3]:\"\"));a=a.target.get(\"linkerParam\");var d=b.indexOf(\"?\");e=b.indexOf(\"#\");return c?b+=(-1==e?\"#\":\"\\x26\")+a:(c=-1==d?\"?\":\"\\x26\",b=-1==e?b+(c+a):b.substring(0,e)+c+a+b.substring(e)),b.replace(\/\u0026+_ga=\/,\n\"\\x26_ga\\x3d\")},Na=function(a,b){if(b\u0026\u0026b.action)if(\"get\"==b.method.toLowerCase()){a=a.target.get(\"linkerParam\").split(\"\\x3d\")[1];for(var c=b.childNodes||[],e=0;e\u003Cc.length;e++)if(\"_ga\"==c[e].name)return void c[e].setAttribute(\"value\",a);c=k.createElement(\"input\");c.setAttribute(\"type\",\"hidden\");c.setAttribute(\"name\",\"_ga\");c.setAttribute(\"value\",a);b.appendChild(c)}else\"post\"==b.method.toLowerCase()\u0026\u0026(b.action=ba(a,b.action))};aa.prototype.S=function(a,b,c){function e(c){try{c=c||p.event;a:{var e=\nc.target||c.srcElement;for(c=100;e\u0026\u00260\u003Cc;){if(e.href\u0026\u0026e.nodeName.match(\/^a(?:rea)?$\/i)){var f=e;break a}e=e.parentNode;c--}f={}}(\"http:\"==f.protocol||\"https:\"==f.protocol)\u0026\u0026sa(a,f.hostname||\"\")\u0026\u0026f.href\u0026\u0026(f.href=ba(d,f.href,b))}catch(Ub){h(26)}}var d=this;this.T||(this.T=!0,S(k,\"mousedown\",e,!1),S(k,\"keyup\",e,!1));c\u0026\u0026S(k,\"submit\",function(b){if(b=b||p.event,(b=b.target||b.srcElement)\u0026\u0026b.action){var c=b.action.match(Jb);c\u0026\u0026sa(a,c[1])\u0026\u0026Na(d,b)}})};var Oa,Mb=function(a,b,c){this.U=ub;this.aa=b;(b=c)||\n(b=(b=m(a,y))\u0026\u0026\"t0\"!=b?Lb.test(b)?\"_gat_\"+fa(m(a,B)):\"_gat_\"+fa(b):\"_gat\");this.Y=b},Pa=function(a,b,c){b.get(c)||(\"1\"==G(a.Y)[0]?b.set(c,\"\",!0):b.set(c,\"\"+R(),!0))},Lb=\/^gtm\\d+$\/,Nb=function(a){if(!a.get(\"dcLoaded\")\u0026\u0026\"cookie\"==a.get(I)){var b=a,c=b;var e=(c=c.get(Ba),\"[object Array]\"==Object.prototype.toString.call(Object(c))||(c=[]),c);e=new Q(e);e.set(51);b.set(Ba,e.w);b=new Mb(a);Pa(b,a,b.U);Pa(b,a,vb);e=b;c=a;c.get(e.U)\u0026\u0026U(e.Y,\"1\",c.get(E),c.get(u),c.get(B),6E4);a.get(b.U)\u0026\u0026(a.set(Gb,1,!0),a.set(Fb,\n\"undefined\/r\/collect\",!0))}},Ob=function(){var a=p.gaGlobal=p.gaGlobal||{};return a.hid=a.hid||R()},Pb=function(a,b,c){if(!Oa){var e=k.location.hash;var d=p.name,f=\/^#?gaso=([^\u0026]*)\/;if(d=(e=(e=e\u0026\u0026e.match(f)||d\u0026\u0026d.match(f))?e[1]:G(\"GASO\")[0]||\"\")\u0026\u0026e.match(\/^(?:!([-0-9a-z.]{1,40})!)?([-.\\w]{10,1200})$\/i))U(\"GASO\",\"\"+e,c,b,a,0),window._udo||(window._udo=b),window._utcp||(window._utcp=c),a=d[1],a=\"https:\/\/www.google.com\/analytics\/web\/inpage\/pub\/inpage.js?\"+(a?\"prefix\\x3d\"+a+\"\\x26\":\"\")+R(),b=\"_gasojs\",\ne=c=void 0,a\u0026\u0026(c?(e=\"\",b\u0026\u0026ua.test(b)\u0026\u0026(e=' id\\x3d\"'+b+'\"'),ua.test(a)\u0026\u0026k.write(\"\\x3cscript\"+e+' src\\x3d\"'+a+'\"\\x3e\\x3c\/script\\x3e')):(c=k.createElement(\"script\"),c.type=\"text\/javascript\",c.async=!0,c.src=a,e\u0026\u0026(c.onload=e),b\u0026\u0026(c.id=b),a=k.getElementsByTagName(\"script\")[0],a.parentNode.insertBefore(c,a)));Oa=!0}},ab=\/^(UA|YT|MO|GP)-(\\d+)-(\\d+)$\/,K=function(a){function b(a,b){e.b.data.set(a,b)}function c(a,c){b(a,c);e.filters.add(a)}var e=this;this.b=new V;this.filters=new wa;b(y,a[y]);b(B,Xa(a[B]));\nb(P,a[P]);b(u,a[u]||T());b(E,a[E]);b(ja,a[ja]);b(Z,a[Z]);b(ka,a[ka]);b(la,a[la]);b(ma,a[ma]);b(Ea,a[Ea]);b(na,a[na]);b(Fa,a[Fa]);b(I,a[I]);b(Da,a[Da]);b(Ca,a[Ca]);b(A,a[A]);b(cb,1);b(db,\"j50\");c(wb,q);c(xb,z);c(yb,r);c(zb,Ua);c(Ab,Hb);c(Bb,Ib);c(Cb,Va);c(Db,F);c(Eb,Nb);Qb(this.b,a[w]);this.b.set(eb,Ob());Pb(this.b.get(B),this.b.get(u),this.b.get(E));this.ra=new pa(1E4,!0,\"gaexp10\")},Qb=function(a,b){if(\"cookie\"==m(a,I)){J=!1;var c=G(m(a,P));if(!(c=Ma(a,c))){c=m(a,u);var e=m(a,Z)||T();c=ca(\"__utma\",\ne,c);void 0!=c?(h(10),c=c.O[1]+\".\"+c.O[2]):c=void 0}c\u0026\u0026(a.data.set(w,c),c=G(m(a,oa)),(c=Ma(a,c))\u0026\u0026a.data.set(H,c),J=!0)}a:if(c=a.get(ma),e=k.location[c?\"href\":\"search\"],c=(e=e.match(\"(?:\\x26|#|\\\\?)\"+M(\"_ga\").replace(\/([.*+?^=!:${}()|\\[\\]\\\/\\\\])\/g,\"\\\\$1\")+\"\\x3d([^\\x26#]*)\"))\u0026\u00262==e.length?e[1]:\"\")if(a.get(la))if(-1==(e=c.indexOf(\".\")))h(22);else{var d=c.substring(0,e),f=c.substring(e+1);e=f.indexOf(\".\");c=f.substring(0,e);f=f.substring(e+1);if(\"1\"==d){if(e=f,c!=C(e,0)\u0026\u0026c!=C(e,-1)\u0026\u0026c!=C(e,-2)){h(23);\nbreak a}}else{if(\"2\"!=d){h(22);break a}if(d=f.split(\"-\",2),e=d[1],c!=C(d[0]+e,0)\u0026\u0026c!=C(d[0]+e,-1)\u0026\u0026c!=C(d[0]+e,-2)){h(53);break a}h(2);a.data.set(H,d[0])}h(11);a.data.set(w,e)}else h(21);b\u0026\u0026(h(9),a.data.set(w,M(b)));a.get(w)||((b=(b=p.gaGlobal\u0026\u0026p.gaGlobal.vid)\u0026\u0026-1!=b.search(\/^(?:utma\\.)?\\d+\\.\\d+$\/)?b:void 0)?(h(17),a.data.set(w,b)):(h(8),a.data.set(w,ta())));a.data.set(A,a.get(A)||1==qa(new pa(0,!0),void 0,L(a.get(w))));a.get(A)\u0026\u0026(b=m(a,P),a.data.set(oa,\"_ga\"==b?\"_gid\":b+\"_gid\"));a.get(A)\u0026\u0026!a.get(H)\u0026\u0026\n(h(3),a.data.set(H,ta()));Ha(a)};K.prototype.get=function(a){return this.b.get(a)};K.prototype.set=function(a,b){this.b.set(a,b)};var Rb={pageview:[fb],event:[gb,hb,ib,jb],social:[kb,lb,mb],timing:[nb,ob,qb,pb]};K.prototype.send=function(a){if(!(1\u003Earguments.length)){var b,c;\"string\"==typeof arguments[0]?(b=arguments[0],c=[].slice.call(arguments,1)):(b=arguments[0]\u0026\u0026arguments[0][Aa],c=arguments);b\u0026\u0026(c=va(Rb[b]||[],c),c[Aa]=b,this.b.set(c,void 0,!0),this.filters.D(this.b),this.b.data.m={},qa(this.ra,\nthis.b)\u0026\u0026this.b.get(B))}};K.prototype.ma=function(a,b){var c=this;c.get(y)};var Qa=function(a){if(\"prerender\"==k.visibilityState||(a(),!1)){h(16);var b=!1,c=function(){if(!b\u0026\u0026\"prerender\"!=k.visibilityState\u0026\u0026(a(),!0)){b=!0;var e=c,d=k;d.removeEventListener?d.removeEventListener(\"visibilitychange\",e,!1):d.detachEvent\u0026\u0026d.detachEvent(\"onvisibilitychange\",e)}};S(k,\"visibilitychange\",c)}},Sb=function(a){};new D;new D;new D;var n={ga:function(){n.f=[]}};n.ga();n.D=function(a){var b=n.J.apply(n,arguments);\nb=n.f.concat(b);for(n.f=[];0\u003Cb.length\u0026\u0026!n.v(b[0])\u0026\u0026(b.shift(),!(0\u003Cn.f.length)););n.f=n.f.concat(b)};n.J=function(a){for(var b=[],c=0;c\u003Carguments.length;c++)try{var d=new Sb(arguments[c]);d.g||(d.i\u0026\u0026(d.ha=void 0),b.push(d))}catch(v){}return b};n.v=function(a){try{if(a.u)a.u.call(p,g.j(\"t0\"));else{var b=a.c==O?g:g.j(a.c);if(a.A)\"t0\"!=a.c||g.create.apply(g,a.a);else if(a.ba)g.remove(a.c);else if(b){if(a.i)return a.ha\u0026\u0026(a.ha=void 0),!0;if(a.K){var c=a.C,d=a.a,h=b.plugins_.get(a.K);h[c].apply(h,d)}else b[a.C].apply(b,\na.a)}}}catch(f){}};var g=function(a){h(1);n.D.apply(n,[arguments])};g.h={};g.P=[];g.L=0;g.answer=42;var Tb=[B,u,y];g.create=function(a){var b=va(Tb,[].slice.call(arguments));b[y]||(b[y]=\"t0\");var c=\"\"+b[y];return g.h[c]?g.h[c]:(b=new K(b),g.h[c]=b,g.P.push(b),b)};g.remove=function(a){for(var b=0;b\u003Cg.P.length;b++)if(g.P[b].get(y)==a){g.P.splice(b,1);g.h[a]=null;break}};g.j=function(a){return g.h[a]};g.getAll=function(){return g.P.slice(0)};g.N=function(){\"ga\"!=O\u0026\u0026h(49);var a=p[O];if(!a||42!=a.answer){g.L=\na\u0026\u0026a.l;g.loaded=!0;var b=p[O]=g;t(\"create\",b,b.create);t(\"remove\",b,b.remove);t(\"getByName\",b,b.j,5);t(\"getAll\",b,b.getAll,6);b=K.prototype;t(\"get\",b,b.get,7);t(\"set\",b,b.set,4);t(\"send\",b,b.send);t(\"requireSync\",b,b.ma);b=V.prototype;t(\"get\",b,b.get);t(\"set\",b,b.set);\"https:\"==k.location.protocol||Y||!qa(new pa(1E4))||(h(36),Y=!0);(p.gaplugins=p.gaplugins||{}).Linker=aa;b=aa.prototype;t(\"decorate\",b,b.ca,20);t(\"autoLink\",b,b.S,25);a=a\u0026\u0026a.q;\"[object Array]\"==Object.prototype.toString.call(Object(a))?\nn.D.apply(g,a):h(50)}};g.da=function(){for(var a=g.getAll(),b=0;b\u003Ca.length;b++)a[b].get(y)};var Ra=g.N,Sa=p[O];Sa\u0026\u0026Sa.r?Ra():Qa(Ra);Qa(function(){n.D([\"provide\",\"render\",Ya])})}(window);var r=\"SCITYLANA\";r=r+\"_temp_\"+Math.round(2147483647*Math.random());q=slga.create(q,z,r);F=F||q.get(\"userId\")||q.get(\"clientId\");slga.remove(r);r=window;q=\"_o_r_d_e_r_sl\";z=(new Date).getTime();r[q]=r[q]?r[q]==z?z+1:z\u003Er[q]?z:r[q]+1:z;return F=[\"sl\\x3d1\",\"u\\x3d\"+F,\"t\\x3d\"+r[q]].join(\"\\x26\")})();"]
    },{
      "function":"__u"
    },{
      "function":"__e"
    },{
      "function":"__v",
      "vtp_name":"ProcessTrial",
      "vtp_defaultValue":"",
      "vtp_setDefaultValue":true,
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"ProcessSale",
      "vtp_defaultValue":"",
      "vtp_setDefaultValue":true,
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"ProcessConversion",
      "vtp_defaultValue":"",
      "vtp_setDefaultValue":true,
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"ProcessSeenCheckout",
      "vtp_defaultValue":"",
      "vtp_setDefaultValue":true,
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"ProcessSeenEntryAny",
      "vtp_defaultValue":"",
      "vtp_setDefaultValue":true,
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"MainOrUser",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"Culture",
      "vtp_defaultValue":"",
      "vtp_setDefaultValue":true,
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"SiteVer",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"Version"
    },{
      "function":"__v",
      "vtp_name":"ExpQS",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"SWStep"
    },{
      "function":"__gas",
      "vtp_cookieDomain":"auto",
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_useHashAutoLink":false,
      "vtp_decorateFormsAutoLink":false,
      "vtp_enableLinkId":false,
      "vtp_enableEcommerce":false,
      "vtp_trackingId":"UA-119897294-1",
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false
    },{
      "function":"__v",
      "vtp_name":"PreOrFre",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"Instrumentation",
      "vtp_defaultValue":"",
      "vtp_setDefaultValue":true,
      "vtp_dataLayerVersion":2
    },{
      "function":"__f"
    },{
      "function":"__u",
      "vtp_component":"QUERY"
    },{
      "function":"__v",
      "vtp_name":"Language",
      "vtp_defaultValue":"",
      "vtp_setDefaultValue":true,
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"ProcessLogin",
      "vtp_defaultValue":"",
      "vtp_setDefaultValue":true,
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"CancelPS",
      "vtp_defaultValue":"",
      "vtp_setDefaultValue":true,
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"EarlyCancelPS",
      "vtp_defaultValue":"",
      "vtp_setDefaultValue":true,
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"EntrySPA",
      "vtp_defaultValue":"",
      "vtp_setDefaultValue":true,
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"NumberOfLogins",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"Email",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"OptiSnippet",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"SalesType",
      "vtp_defaultValue":"",
      "vtp_setDefaultValue":true,
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"RefCode",
      "vtp_defaultValue":"",
      "vtp_setDefaultValue":true,
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"PreviewPage",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"RecognizedCustomer",
      "vtp_defaultValue":"",
      "vtp_setDefaultValue":true,
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"Instrumentation",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"Segmentation",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"BlogUser",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"Market",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_name":"CancelCheckout",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_setDefaultValue":false,
      "vtp_name":"ProcessSeenEntryAny",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_setDefaultValue":false,
      "vtp_name":"ProcessTrial",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_setDefaultValue":false,
      "vtp_name":"target",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_setDefaultValue":true,
      "vtp_name":"ConversionValue",
      "vtp_defaultValue":"",
      "vtp_dataLayerVersion":2
    },{
      "function":"__v",
      "vtp_setDefaultValue":false,
      "vtp_name":"ConversionCurrency",
      "vtp_dataLayerVersion":2
    },{
      "function":"__smm",
      "vtp_setDefaultValue":true,
      "vtp_input":["macro",42],
      "vtp_defaultValue":"650",
      "vtp_map":["list",["map","key","AE","value","400"],["map","key","AF","value","250"],["map","key","AG","value","400"],["map","key","AL","value","250"],["map","key","AM","value","400"],["map","key","AO","value","200"],["map","key","AR","value","350"],["map","key","AS","value","250"],["map","key","AT","value","500"],["map","key","AU","value","450"],["map","key","AZ","value","500"],["map","key","BA","value","400"],["map","key","BB","value","400"],["map","key","BD","value","250"],["map","key","BE","value","800"],["map","key","BF","value","200"],["map","key","BG","value","400"],["map","key","BH","value","500"],["map","key","BI","value","200"],["map","key","BJ","value","200"],["map","key","BM","value","400"],["map","key","BN","value","400"],["map","key","BO","value","250"],["map","key","BQ","value","370"],["map","key","BR","value","300"],["map","key","BS","value","400"],["map","key","BT","value","250"],["map","key","BW","value","300"],["map","key","BY","value","500"],["map","key","BZ","value","400"],["map","key","CA","value","500"],["map","key","CD","value","300"],["map","key","CG","value","200"],["map","key","CH","value","1000"],["map","key","CI","value","300"],["map","key","CL","value","400"],["map","key","CM","value","300"],["map","key","CN","value","350"],["map","key","CO","value","400"],["map","key","CR","value","400"],["map","key","CV","value","200"],["map","key","CW","value","400"],["map","key","CY","value","400"],["map","key","CZ","value","350"],["map","key","DE","value","600"],["map","key","DJ","value","200"],["map","key","DK","value","1100"],["map","key","DM","value","400"],["map","key","DO","value","400"],["map","key","DZ","value","300"],["map","key","EC","value","400"],["map","key","EE","value","400"],["map","key","EG","value","300"],["map","key","ES","value","400"],["map","key","ET","value","200"],["map","key","FI","value","700"],["map","key","FJ","value","250"],["map","key","FK","value","400"],["map","key","FO","value","700"],["map","key","FR","value","600"],["map","key","GA","value","300"],["map","key","GB","value","600"],["map","key","GD","value","400"],["map","key","GE","value","500"],["map","key","GH","value","250"],["map","key","GN","value","200"],["map","key","GP","value","700"],["map","key","GR","value","400"],["map","key","GT","value","400"],["map","key","GW","value","200"],["map","key","GY","value","350"],["map","key","HK","value","450"],["map","key","HN","value","400"],["map","key","HR","value","400"],["map","key","HT","value","400"],["map","key","HU","value","400"],["map","key","ID","value","350"],["map","key","IE","value","400"],["map","key","IL","value","500"],["map","key","IN","value","250"],["map","key","IQ","value","300"],["map","key","IR","value","370"],["map","key","IS","value","400"],["map","key","IT","value","450"],["map","key","JM","value","400"],["map","key","JO","value","300"],["map","key","JP","value","400"],["map","key","KE","value","250"],["map","key","KG","value","250"],["map","key","KH","value","250"],["map","key","KM","value","200"],["map","key","KR","value","400"],["map","key","KW","value","350"],["map","key","KY","value","400"],["map","key","KZ","value","350"],["map","key","LA","value","250"],["map","key","LB","value","350"],["map","key","LC","value","400"],["map","key","LI","value","800"],["map","key","LK","value","300"],["map","key","LR","value","200"],["map","key","LT","value","400"],["map","key","LU","value","600"],["map","key","LV","value","400"],["map","key","LY","value","200"],["map","key","MA","value","300"],["map","key","MC","value","800"],["map","key","MD","value","400"],["map","key","ME","value","400"],["map","key","MG","value","200"],["map","key","MK","value","400"],["map","key","ML","value","200"],["map","key","MM","value","250"],["map","key","MN","value","250"],["map","key","MO","value","400"],["map","key","MQ","value","400"],["map","key","MR","value","200"],["map","key","MT","value","500"],["map","key","MU","value","250"],["map","key","MV","value","250"],["map","key","MX","value","400"],["map","key","MY","value","300"],["map","key","MZ","value","200"],["map","key","NA","value","200"],["map","key","NC","value","300"],["map","key","NG","value","200"],["map","key","NI","value","400"],["map","key","NL","value","750"],["map","key","NO","value","800"],["map","key","NP","value","250"],["map","key","NZ","value","400"],["map","key","OM","value","300"],["map","key","PA","value","400"],["map","key","PE","value","350"],["map","key","PF","value","250"],["map","key","PG","value","300"],["map","key","PH","value","350"],["map","key","PK","value","300"],["map","key","PL","value","400"],["map","key","PR","value","400"],["map","key","PT","value","400"],["map","key","PY","value","400"],["map","key","QA","value","500"],["map","key","RE","value","370"],["map","key","RO","value","400"],["map","key","RS","value","400"],["map","key","RU","value","143"],["map","key","RW","value","200"],["map","key","SA","value","450"],["map","key","SD","value","370"],["map","key","SE","value","850"],["map","key","SG","value","350"],["map","key","SI","value","400"],["map","key","SK","value","400"],["map","key","SN","value","250"],["map","key","SO","value","200"],["map","key","SR","value","350"],["map","key","ST","value","200"],["map","key","SV","value","400"],["map","key","SY","value","370"],["map","key","TC","value","400"],["map","key","TG","value","300"],["map","key","TH","value","400"],["map","key","TJ","value","250"],["map","key","TL","value","250"],["map","key","TM","value","250"],["map","key","TN","value","300"],["map","key","TR","value","250"],["map","key","TT","value","400"],["map","key","TW","value","450"],["map","key","TZ","value","250"],["map","key","UA","value","350"],["map","key","UG","value","300"],["map","key","US","value","400"],["map","key","UY","value","400"],["map","key","UZ","value","250"],["map","key","VA","value","400"],["map","key","VE","value","400"],["map","key","VI","value","400"],["map","key","VN","value","300"],["map","key","XK","value","400"],["map","key","YE","value","250"],["map","key","YT","value","200"],["map","key","ZA","value","400"],["map","key","ZM","value","350"],["map","key","ZW","value","200"]]
    },{
      "function":"__r"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"NumberOfLogins"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"NumberOfLogins"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"Email"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"CustomerId"
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){var a=!1;try{var b=Math.floor((Date.now()-Date.UTC(2016,7,8,7,0,0,0))\/1E3\/60),c=Math.floor((Date.now()-Date.UTC(2016,7,12,21,59,0,0))\/1E3\/60);a=",["escape",["macro",7],8,16],"\u003C=b;nottoofresh=",["escape",["macro",7],8,16],"\u003E=c}catch(d){}return a\u0026\u0026nottoofresh})();"]
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"IsMobile"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"ProcessTrial"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"Version"
    },{
      "function":"__gas",
      "vtp_cookieDomain":"auto",
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_useHashAutoLink":false,
      "vtp_decorateFormsAutoLink":false,
      "vtp_enableLinkId":false,
      "vtp_dimension":["list",["map","index","18","dimension",["macro",8]]],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":"UA-48375203-3",
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false
    },{
      "function":"__v",
      "vtp_dataLayerVersion":1,
      "vtp_setDefaultValue":false,
      "vtp_name":"Message"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"Segmentation"
    },{
      "function":"__gas",
      "vtp_cookieDomain":"auto",
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_useHashAutoLink":false,
      "vtp_decorateFormsAutoLink":false,
      "vtp_enableLinkId":false,
      "vtp_enableEcommerce":false,
      "vtp_trackingId":"UA-119140735-1",
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false
    },{
      "function":"__u",
      "vtp_component":"URL"
    }],
  "tags":[{
      "function":"__paused",
      "vtp_originalTagType":"ua",
      "tag_id":25
    },{
      "function":"__paused",
      "vtp_originalTagType":"ua",
      "tag_id":38
    },{
      "function":"__paused",
      "vtp_originalTagType":"ua",
      "tag_id":39
    },{
      "function":"__paused",
      "vtp_originalTagType":"ua",
      "tag_id":40
    },{
      "function":"__paused",
      "vtp_originalTagType":"ua",
      "tag_id":208
    },{
      "function":"__paused",
      "vtp_originalTagType":"ua",
      "tag_id":210
    },{
      "function":"__paused",
      "vtp_originalTagType":"ua",
      "tag_id":236
    },{
      "function":"__paused",
      "vtp_originalTagType":"html",
      "tag_id":295
    },{
      "function":"__paused",
      "vtp_originalTagType":"ua",
      "tag_id":342
    },{
      "function":"__paused",
      "vtp_originalTagType":"ga",
      "tag_id":353
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_overrideGaSettings":true,
      "vtp_trackType":"TRACK_PAGEVIEW",
      "vtp_gaSettings":["macro",22],
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "tag_id":381
    },{
      "function":"__paused",
      "vtp_originalTagType":"awct",
      "tag_id":382
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Evar d,expires,cvalue=\"\";",["escape",["macro",1],8,16],"\u0026\u0026(d=new Date,d.setTime(d.getTime()+6E5),expires=\"expires\\x3d\"+d.toUTCString(),document.cookie=\"customer_id_cookie\\x3d\"+cvalue+",["escape",["macro",1],8,16],"+\"; \"+expires+\"; path\\x3d\/\");\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":287
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E0\u003C=document.cookie.indexOf(\"customer_id_cookie\")?dataLayer.push({event:\"customerId-cookie-found\"}):dataLayer.push({event:\"customerId-cookie-not-found\"});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":288
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003Evar d,expires,cvalue=\"\";",["escape",["macro",1],8,16],"\u0026\u0026(d=new Date,d.setTime(d.getTime()+6E5),expires=\"expires\\x3d\"+d.toUTCString(),document.cookie=\"free_trial_customer_id_cookie\\x3d\"+cvalue+",["escape",["macro",1],8,16],"+\"; \"+expires+\"; path\\x3d\/\");\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":290
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E0\u003C=document.cookie.indexOf(\"free_trial_customer_id_cookie\")?dataLayer.push({event:\"free-trial-customerId-cookie-found\"}):dataLayer.push({event:\"free-trial-customerId-cookie-not-found\"});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":291
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\n\u003Cscript type=\"text\/gtmscript\"\u003E(function(e,b,f,g){var c,d,a;e.SMCX=e.SMCX||[];b.getElementById(g)||(c=b.getElementsByTagName(f),d=c[c.length-1],a=b.createElement(f),a.type=\"text\/javascript\",a.async=!0,a.id=g,a.src=[\"https:\"===location.protocol?\"https:\/\/\":\"http:\/\/\",\"widget.surveymonkey.com\/collect\/website\/js\/oGV2Ywcf6Y89wItD87kGvLDEiam9qEEQtFtUZm_2FWUTPWU8SLUdSt0uu9bZh2HWK_2B.js\"].join(\"\"),d.parentNode.insertBefore(a,d))})(window,document,\"script\",\"smcx-sdk\");\u003C\/script\u003E\n",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":313
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\n\u003Cscript type=\"text\/gtmscript\"\u003E(function(e,b,f,g){var c,d,a;e.SMCX=e.SMCX||[];b.getElementById(g)||(c=b.getElementsByTagName(f),d=c[c.length-1],a=b.createElement(f),a.type=\"text\/javascript\",a.async=!0,a.id=g,a.src=[\"https:\"===location.protocol?\"https:\/\/\":\"http:\/\/\",\"widget.surveymonkey.com\/collect\/website\/js\/c8osvoco2O9s9KaFYbALq5jz_2F_2BHo2v318TXoXP3ie9KL9TCOk54974RZqb5MDoPL.js\"].join(\"\"),d.parentNode.insertBefore(a,d))})(window,document,\"script\",\"smcx-sdk\");\u003C\/script\u003E\n",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":314
    }],
  "predicates":[{
      "function":"_re",
      "arg0":["macro",9],
      "arg1":".*"
    },{
      "function":"_eq",
      "arg0":["macro",10],
      "arg1":"gtm.js"
    },{
      "function":"_eq",
      "arg0":["macro",11],
      "arg1":"True"
    },{
      "function":"_eq",
      "arg0":["macro",12],
      "arg1":"True"
    },{
      "function":"_eq",
      "arg0":["macro",13],
      "arg1":"True"
    },{
      "function":"_eq",
      "arg0":["macro",14],
      "arg1":"True"
    },{
      "function":"_eq",
      "arg0":["macro",15],
      "arg1":"True"
    },{
      "function":"_eq",
      "arg0":["macro",16],
      "arg1":"MainSite"
    },{
      "function":"_eq",
      "arg0":["macro",17],
      "arg1":"en-US"
    },{
      "function":"_eq",
      "arg0":["macro",18],
      "arg1":"US"
    },{
      "function":"_eq",
      "arg0":["macro",19],
      "arg1":"V4"
    },{
      "function":"_cn",
      "arg0":["macro",20],
      "arg1":"V4EditormodelExperiment"
    },{
      "function":"_eq",
      "arg0":["macro",10],
      "arg1":"gtm.load"
    },{
      "function":"_re",
      "arg0":["macro",21],
      "arg1":"(^$)|(\\s+$)"
    },{
      "function":"_eq",
      "arg0":["macro",21],
      "arg1":"undefined"
    },{
      "function":"_re",
      "arg0":["macro",1],
      "arg1":"[0-9]"
    },{
      "function":"_cn",
      "arg0":["macro",16],
      "arg1":"Userpage"
    },{
      "function":"_eq",
      "arg0":["macro",10],
      "arg1":"customerId-cookie-not-found"
    },{
      "function":"_eq",
      "arg0":["macro",10],
      "arg1":"free-trial-customerId-cookie-not-found"
    },{
      "function":"_eq",
      "arg0":["macro",23],
      "arg1":"Free"
    },{
      "function":"_eq",
      "arg0":["macro",17],
      "arg1":"''"
    },{
      "function":"_eq",
      "arg0":["macro",24],
      "arg1":"''"
    },{
      "function":"_lt",
      "arg0":["macro",7],
      "arg1":"20"
    },{
      "function":"_ge",
      "arg0":["macro",7],
      "arg1":"1"
    },{
      "function":"_eq",
      "arg0":["macro",7],
      "arg1":"''"
    },{
      "function":"_eq",
      "arg0":["macro",23],
      "arg1":"''"
    },{
      "function":"_cn",
      "arg0":["macro",20],
      "arg1":"BuiltToPurpose2Experiment=BuiltToPurpose2"
    },{
      "function":"_eq",
      "arg0":["macro",10],
      "arg1":"gtm.dom"
    },{
      "function":"_cn",
      "arg0":["macro",20],
      "arg1":"BuiltToPurpose2Experiment=Regular"
    }],
  "rules":[
    [["if",0,1],["add",0]],
    [["if",1,2],["add",1,11,15]],
    [["if",1,3],["add",2]],
    [["if",1,4],["add",3,13]],
    [["if",1,5],["add",4]],
    [["if",1,6],["add",5]],
    [["if",1,7],["add",6]],
    [["if",8,9,10,11,12],["add",7]],
    [["if",1,7],["unless",13,14],["add",8]],
    [["if",1],["unless",15,16],["add",9]],
    [["if",1],["add",10]],
    [["if",17],["add",12]],
    [["if",18],["add",14]],
    [["if",7,8,9,19,22,23,26,27],["unless",10,20,21,24,25],["add",16]],
    [["if",7,8,9,19,22,23,27,28],["unless",10,20,21,24,25],["add",17]]]
},
"runtime":[
[],[]
]


};
var da,ea=this,fa=/^[\w+/_-]+[=]{0,2}$/,ia=null,la=function(a,b){function c(){}c.prototype=b.prototype;a.Xg=b.prototype;a.prototype=new c;a.prototype.constructor=a;a.Gg=function(a,c,g){for(var d=Array(arguments.length-2),e=2;e<arguments.length;e++)d[e-2]=arguments[e];return b.prototype[c].apply(a,d)}};var ma=function(){},na=function(a){return"function"==typeof a},f=function(a){return"string"==typeof a},oa=function(a){return"number"==typeof a&&!isNaN(a)},qa=function(a){return"[object Array]"==Object.prototype.toString.call(Object(a))},n=function(a,b){if(Array.prototype.indexOf){var c=a.indexOf(b);return"number"==typeof c?c:-1}for(var d=0;d<a.length;d++)if(a[d]===b)return d;return-1},ra=function(a,b){if(a&&qa(a))for(var c=0;c<a.length;c++)if(a[c]&&b(a[c]))return a[c]},sa=function(a,b){if(!oa(a)||
!oa(b)||a>b)a=0,b=2147483647;return Math.floor(Math.random()*(b-a+1)+a)},ta=function(a,b){for(var c in a)Object.prototype.hasOwnProperty.call(a,c)&&b(c,a[c])},ua=function(a){return Math.round(Number(a))||0},va=function(a){return"false"==String(a).toLowerCase()?!1:!!a},wa=function(a){var b=[];if(qa(a))for(var c=0;c<a.length;c++)b.push(String(a[c]));return b},xa=function(a){return a?a.replace(/^\s+|\s+$/g,""):""},ya=function(){return(new Date).getTime()},za=function(){this.prefix="gtm.";this.values=
{}};za.prototype.set=function(a,b){this.values[this.prefix+a]=b};za.prototype.get=function(a){return this.values[this.prefix+a]};za.prototype.contains=function(a){return void 0!==this.get(a)};
var Ba=function(a,b,c){return a&&a.hasOwnProperty(b)?a[b]:c},Ca=function(a){var b=!1;return function(){if(!b)try{a()}catch(c){}b=!0}},Da=function(a,b){for(var c in b)b.hasOwnProperty(c)&&(a[c]=b[c])},Fa=function(a){for(var b in a)if(a.hasOwnProperty(b))return!0;return!1},Ga=function(a,b){for(var c=[],d=0;d<a.length;d++)c.push(a[d]),c.push.apply(c,b[a[d]]||[]);return c};/*
 jQuery v1.9.1 (c) 2005, 2012 jQuery Foundation, Inc. jquery.org/license. */
var Ha=/\[object (Boolean|Number|String|Function|Array|Date|RegExp)\]/,Ia=function(a){if(null==a)return String(a);var b=Ha.exec(Object.prototype.toString.call(Object(a)));return b?b[1].toLowerCase():"object"},Ja=function(a,b){return Object.prototype.hasOwnProperty.call(Object(a),b)},Ka=function(a){if(!a||"object"!=Ia(a)||a.nodeType||a==a.window)return!1;try{if(a.constructor&&!Ja(a,"constructor")&&!Ja(a.constructor.prototype,"isPrototypeOf"))return!1}catch(c){return!1}for(var b in a);return void 0===
b||Ja(a,b)},t=function(a,b){var c=b||("array"==Ia(a)?[]:{}),d;for(d in a)if(Ja(a,d)){var e=a[d];"array"==Ia(e)?("array"!=Ia(c[d])&&(c[d]=[]),c[d]=t(e,c[d])):Ka(e)?(Ka(c[d])||(c[d]={}),c[d]=t(e,c[d])):c[d]=e}return c};var x=window,y=document,La=navigator,Ma=y.currentScript&&y.currentScript.src,Na=function(a,b){var c=x[a];x[a]=void 0===c?b:c;return x[a]},Oa=function(a,b){b&&(a.addEventListener?a.onload=b:a.onreadystatechange=function(){a.readyState in{loaded:1,complete:1}&&(a.onreadystatechange=null,b())})},Pa=function(a,b,c){var d=y.createElement("script");d.type="text/javascript";d.async=!0;d.src=a;Oa(d,b);c&&(d.onerror=c);var e;if(null===ia)b:{var g=ea.document,h=g.querySelector&&g.querySelector("script[nonce]");
if(h){var k=h.nonce||h.getAttribute("nonce");if(k&&fa.test(k)){ia=k;break b}}ia=""}e=ia;e&&d.setAttribute("nonce",e);var l=y.getElementsByTagName("script")[0]||y.body||y.head;l.parentNode.insertBefore(d,l);return d},Qa=function(){if(Ma){var a=Ma.toLowerCase();if(0===a.indexOf("https://"))return 2;if(0===a.indexOf("http://"))return 3}return 1},Ra=function(a,b){var c=y.createElement("iframe");c.height="0";c.width="0";c.style.display="none";c.style.visibility="hidden";var d=y.body&&y.body.lastChild||
y.body||y.head;d.parentNode.insertBefore(c,d);Oa(c,b);void 0!==a&&(c.src=a);return c},Sa=function(a,b,c){var d=new Image(1,1);d.onload=function(){d.onload=null;b&&b()};d.onerror=function(){d.onerror=null;c&&c()};d.src=a;return d},A=function(a,b,c,d){a.addEventListener?a.addEventListener(b,c,!!d):a.attachEvent&&a.attachEvent("on"+b,c)},Ta=function(a,b,c){a.removeEventListener?a.removeEventListener(b,c,!1):a.detachEvent&&a.detachEvent("on"+b,c)},C=function(a){x.setTimeout(a,0)},Va=function(a){var b=
y.getElementById(a);if(b&&Ua(b,"id")!=a)for(var c=1;c<document.all[a].length;c++)if(Ua(document.all[a][c],"id")==a)return document.all[a][c];return b},Ua=function(a,b){return a&&b&&a.attributes&&a.attributes[b]?a.attributes[b].value:null},Wa=function(a){var b=a.innerText||a.textContent||"";b&&" "!=b&&(b=b.replace(/^[\s\xa0]+|[\s\xa0]+$/g,""));b&&(b=b.replace(/(\xa0+|\s{2,}|\n|\r\t)/g," "));return b},Xa=function(a){var b=y.createElement("div");b.innerHTML="A<div>"+a+"</div>";b=b.lastChild;for(var c=
[];b.firstChild;)c.push(b.removeChild(b.firstChild));return c},Ya=function(a,b,c){c=c||100;for(var d={},e=0;e<b.length;e++)d[b[e]]=!0;for(var g=a,h=0;g&&h<=c;h++){if(d[String(g.tagName).toLowerCase()])return g;g=g.parentElement}return null};var Za=/^(?:(?:https?|mailto|ftp):|[^:/?#]*(?:[/?#]|$))/i;var $a=/:[0-9]+$/,cb=function(a,b,c){for(var d=a.split("&"),e=0;e<d.length;e++){var g=d[e].split("=");if(decodeURIComponent(g[0]).replace(/\+/g," ")===b){var h=g.slice(1).join("=");return c?h:decodeURIComponent(h).replace(/\+/g," ")}}},D=function(a,b,c,d,e){b&&(b=String(b).toLowerCase());if("protocol"===b||"port"===b)a.protocol=db(a.protocol)||db(x.location.protocol);"port"===b?a.port=String(Number(a.hostname?a.port:x.location.port)||("http"==a.protocol?80:"https"==a.protocol?443:"")):"host"===b&&
(a.hostname=(a.hostname||x.location.hostname).replace($a,"").toLowerCase());var g=b,h,k=db(a.protocol);g&&(g=String(g).toLowerCase());switch(g){case "url_no_fragment":h=eb(a);break;case "protocol":h=k;break;case "host":h=a.hostname.replace($a,"").toLowerCase();if(c){var l=/^www\d*\./.exec(h);l&&l[0]&&(h=h.substr(l[0].length))}break;case "port":h=String(Number(a.port)||("http"==k?80:"https"==k?443:""));break;case "path":h="/"==a.pathname.substr(0,1)?a.pathname:"/"+a.pathname;var m=h.split("/");0<=
n(d||[],m[m.length-1])&&(m[m.length-1]="");h=m.join("/");break;case "query":h=a.search.replace("?","");e&&(h=cb(h,e,void 0));break;case "extension":var p=a.pathname.split(".");h=1<p.length?p[p.length-1]:"";h=h.split("/")[0];break;case "fragment":h=a.hash.replace("#","");break;default:h=a&&a.href}return h},db=function(a){return a?a.replace(":","").toLowerCase():""},eb=function(a){var b="";if(a&&a.href){var c=a.href.indexOf("#");b=0>c?a.href:a.href.substr(0,c)}return b},fb=function(a){var b=y.createElement("a");
a&&(b.href=a);var c=b.pathname;"/"!==c[0]&&(c="/"+c);var d=b.hostname.replace($a,"");return{href:b.href,protocol:b.protocol,host:b.host,hostname:d,pathname:c,search:b.search,hash:b.hash,port:b.port}};var gb=function(a,b,c){for(var d=[],e=String(b||document.cookie).split(";"),g=0;g<e.length;g++){var h=e[g].split("="),k=h[0].replace(/^\s*|\s*$/g,"");if(k&&k==a){var l=h.slice(1).join("=").replace(/^\s*|\s*$/g,"");l&&c&&(l=decodeURIComponent(l));d.push(l)}}return d},jb=function(a,b,c,d){var e=hb(a,d);if(1===e.length)return e[0].id;if(0!==e.length){e=ib(e,function(a){return a.yb},b);if(1===e.length)return e[0].id;e=ib(e,function(a){return a.Ta},c);return e[0]?e[0].id:void 0}};
function kb(a,b,c){var d=document.cookie;document.cookie=a;var e=document.cookie;return d!=e||void 0!=c&&0<=gb(b,e).indexOf(c)}
var nb=function(a,b,c,d,e,g){d=d||"auto";var h={path:c||"/"};e&&(h.expires=e);"none"!==d&&(h.domain=d);var k;a:{var l=b,m;if(void 0==l)m=a+"=deleted; expires="+(new Date(0)).toUTCString();else{g&&(l=encodeURIComponent(l));var p=l;p&&1200<p.length&&(p=p.substring(0,1200));l=p;m=a+"="+l}var q=void 0,r=void 0,w;for(w in h)if(h.hasOwnProperty(w)){var v=h[w];if(null!=v)switch(w){case "secure":v&&(m+="; secure");break;case "domain":q=v;break;default:"path"==w&&(r=v),"expires"==w&&v instanceof Date&&(v=
v.toUTCString()),m+="; "+w+"="+v}}if("auto"===q){for(var u=lb(),z=0;z<u.length;++z){var E="none"!=u[z]?u[z]:void 0;if(!mb(E,r)&&kb(m+(E?"; domain="+E:""),a,l)){k=!0;break a}}k=!1}else q&&"none"!=q&&(m+="; domain="+q),k=!mb(q,r)&&kb(m,a,l)}return k};function ib(a,b,c){for(var d=[],e=[],g,h=0;h<a.length;h++){var k=a[h],l=b(k);l===c?d.push(k):void 0===g||l<g?(e=[k],g=l):l===g&&e.push(k)}return 0<d.length?d:e}
function hb(a,b){for(var c=[],d=gb(a),e=0;e<d.length;e++){var g=d[e].split("."),h=g.shift();if(!b||-1!==b.indexOf(h)){var k=g.shift();k&&(k=k.split("-"),c.push({id:g.join("."),yb:1*k[0]||1,Ta:1*k[1]||1}))}}return c}
var ob=/^(www\.)?google(\.com?)?(\.[a-z]{2})?$/,pb=/(^|\.)doubleclick\.net$/i,mb=function(a,b){return pb.test(document.location.hostname)||"/"===b&&ob.test(a)},lb=function(){var a=[],b=document.location.hostname.split(".");if(4===b.length){var c=b[b.length-1];if(parseInt(c,10).toString()===c)return["none"]}for(var d=b.length-2;0<=d;d--)a.push(b.slice(d).join("."));a.push("none");return a};
var qb=[],rb={"\x00":"&#0;",'"':"&quot;","&":"&amp;","'":"&#39;","<":"&lt;",">":"&gt;","\t":"&#9;","\n":"&#10;","\x0B":"&#11;","\f":"&#12;","\r":"&#13;"," ":"&#32;","-":"&#45;","/":"&#47;","=":"&#61;","`":"&#96;","\u0085":"&#133;","\u00a0":"&#160;","\u2028":"&#8232;","\u2029":"&#8233;"},sb=function(a){return rb[a]},tb=/[\x00\x22\x26\x27\x3c\x3e]/g;var xb=/[\x00\x08-\x0d\x22\x26\x27\/\x3c-\x3e\\\x85\u2028\u2029]/g,yb={"\x00":"\\x00","\b":"\\x08","\t":"\\t","\n":"\\n","\x0B":"\\x0b",
"\f":"\\f","\r":"\\r",'"':"\\x22","&":"\\x26","'":"\\x27","/":"\\/","<":"\\x3c","=":"\\x3d",">":"\\x3e","\\":"\\\\","\u0085":"\\x85","\u2028":"\\u2028","\u2029":"\\u2029",$:"\\x24","(":"\\x28",")":"\\x29","*":"\\x2a","+":"\\x2b",",":"\\x2c","-":"\\x2d",".":"\\x2e",":":"\\x3a","?":"\\x3f","[":"\\x5b","]":"\\x5d","^":"\\x5e","{":"\\x7b","|":"\\x7c","}":"\\x7d"},zb=function(a){return yb[a]};
qb[8]=function(a){if(null==a)return" null ";switch(typeof a){case "boolean":case "number":return" "+a+" ";default:return"'"+String(String(a)).replace(xb,zb)+"'"}};var Ab=/[\x00\x08-\x0d\x22\x24\x26-\/\x3a\x3c-\x3f\x5b-\x5e\x7b-\x7d\x85\u2028\u2029]/g;qb[9]=function(a){return String(a).replace(Ab,zb)};var Hb=/[\x00- \x22\x27-\x29\x3c\x3e\\\x7b\x7d\x7f\x85\xa0\u2028\u2029\uff01\uff03\uff04\uff06-\uff0c\uff0f\uff1a\uff1b\uff1d\uff1f\uff20\uff3b\uff3d]/g,Ib={"\x00":"%00","\u0001":"%01","\u0002":"%02","\u0003":"%03","\u0004":"%04","\u0005":"%05","\u0006":"%06","\u0007":"%07","\b":"%08","\t":"%09","\n":"%0A","\x0B":"%0B","\f":"%0C","\r":"%0D","\u000e":"%0E","\u000f":"%0F","\u0010":"%10",
"\u0011":"%11","\u0012":"%12","\u0013":"%13","\u0014":"%14","\u0015":"%15","\u0016":"%16","\u0017":"%17","\u0018":"%18","\u0019":"%19","\u001a":"%1A","\u001b":"%1B","\u001c":"%1C","\u001d":"%1D","\u001e":"%1E","\u001f":"%1F"," ":"%20",'"':"%22","'":"%27","(":"%28",")":"%29","<":"%3C",">":"%3E","\\":"%5C","{":"%7B","}":"%7D","\u007f":"%7F","\u0085":"%C2%85","\u00a0":"%C2%A0","\u2028":"%E2%80%A8","\u2029":"%E2%80%A9","\uff01":"%EF%BC%81","\uff03":"%EF%BC%83","\uff04":"%EF%BC%84","\uff06":"%EF%BC%86",
"\uff07":"%EF%BC%87","\uff08":"%EF%BC%88","\uff09":"%EF%BC%89","\uff0a":"%EF%BC%8A","\uff0b":"%EF%BC%8B","\uff0c":"%EF%BC%8C","\uff0f":"%EF%BC%8F","\uff1a":"%EF%BC%9A","\uff1b":"%EF%BC%9B","\uff1d":"%EF%BC%9D","\uff1f":"%EF%BC%9F","\uff20":"%EF%BC%A0","\uff3b":"%EF%BC%BB","\uff3d":"%EF%BC%BD"},Jb=function(a){return Ib[a]};qb[16]=function(a){return a};var Lb=[],Mb=[],Nb=[],Ob=[],Pb=[],Qb={},Rb,Sb,Tb,Ub=function(a,b){var c={};c["function"]="__"+a;for(var d in b)b.hasOwnProperty(d)&&(c["vtp_"+d]=b[d]);return c},Vb=function(a,b){var c=a["function"];if(!c)throw Error("Error: No function name given for function call.");var d=!!Qb[c],e={},g;for(g in a)a.hasOwnProperty(g)&&0===g.indexOf("vtp_")&&(e[d?g:g.substr(4)]=a[g]);return d?Qb[c](e):(void 0)(c,e,b)},Xb=function(a,b,c,d){c=c||[];d=d||ma;var e={},g;for(g in a)a.hasOwnProperty(g)&&(e[g]=Wb(a[g],b,
c,d));return e},Yb=function(a){var b=a["function"];if(!b)throw"Error: No function name given for function call.";var c=Qb[b];return c?c.b||0:0},Wb=function(a,b,c,d){if(qa(a)){var e;switch(a[0]){case "function_id":return a[1];case "list":e=[];for(var g=1;g<a.length;g++)e.push(Wb(a[g],b,c,d));return e;case "macro":var h=a[1];if(c[h])return;var k=Lb[h];if(!k||b(k))return;c[h]=!0;try{var l=Xb(k,b,c,d);e=Vb(l,d);Tb&&(e=Tb.ff(e,l))}catch(E){d(E,h),e=!1}c[h]=!1;return e;case "map":e={};for(var m=1;m<a.length;m+=
2)e[Wb(a[m],b,c,d)]=Wb(a[m+1],b,c,d);return e;case "template":e=[];for(var p=!1,q=1;q<a.length;q++){var r=Wb(a[q],b,c,d);Sb&&(p=p||r===Sb.ob);e.push(r)}return Sb&&p?Sb.kf(e):e.join("");case "escape":e=Wb(a[1],b,c,d);if(Sb&&qa(a[1])&&"macro"===a[1][0]&&Sb.Nf(a))return Sb.Yf(e);e=String(e);for(var w=2;w<a.length;w++)qb[a[w]]&&(e=qb[a[w]](e));return e;case "tag":var v=a[1];if(!Ob[v])throw Error("Unable to resolve tag reference "+v+".");return e={wd:a[2],index:v};case "zb":var u={arg0:a[2],arg1:a[3],
ignore_case:a[5]};u["function"]=a[1];var z=Zb(u,b,c,d);a[4]&&(z=!z);return z;default:throw Error("Attempting to expand unknown Value type: "+a[0]+".");}}return a},Zb=function(a,b,c,d){try{return Rb(Xb(a,b,c,d))}catch(e){JSON.stringify(a)}return null};var $b=null,cc=function(a,b){function c(a){for(var b=0;b<a.length;b++)e[a[b]]=!0}var d=[],e=[];$b=ac(a,b||function(){});for(var g=0;g<Mb.length;g++){var h=Mb[g],k=bc(h);if(k){for(var l=h.add||[],m=0;m<l.length;m++)d[l[m]]=!0;c(h.block||[])}else null===k&&c(h.block||[])}for(var p=[],q=0;q<Ob.length;q++)d[q]&&!e[q]&&(p[q]=!0);return p},bc=function(a){for(var b=a["if"]||[],c=0;c<b.length;c++){var d=$b(b[c]);if(!d)return null===d?null:!1}for(var e=a.unless||[],g=0;g<e.length;g++){var h=$b(e[g]);if(null===
h)return null;if(h)return!1}return!0},ac=function(a,b){var c=[];return function(d){void 0===c[d]&&(c[d]=Zb(Nb[d],a,void 0,b));return c[d]}};/*
 Copyright (c) 2014 Derek Brans, MIT license https://github.com/krux/postscribe/blob/master/LICENSE. Portions derived from simplehtmlparser, which is licensed under the Apache License, Version 2.0 */
var H={},K=null,pc=Math.random();H.m="GTM-2MMH";H.sb="3b2";var qc="www.googletagmanager.com/gtm.js";var rc=qc,sc=null,tc=null,uc=null,vc="//www.googletagmanager.com/a?id="+H.m+"&cv=348",wc={},xc={},yc=function(){var a=K.sequence||0;K.sequence=a+1;return a};var O=function(a,b,c,d){return(2===zc()||d||"http:"!=x.location.protocol?a:b)+c},zc=function(){var a=Qa(),b;if(1===a)a:{var c=rc;c=c.toLowerCase();for(var d="https://"+c,e="http://"+c,g=1,h=y.getElementsByTagName("script"),k=0;k<h.length&&100>k;k++){var l=h[k].src;if(l){l=l.toLowerCase();if(0===l.indexOf(e)){b=3;break a}1===g&&0===l.indexOf(d)&&(g=2)}}b=g}else b=a;return b};var Ac=!1;var P=function(){var a=function(a){return{toString:function(){return a}}};return{Vc:a("convert_case_to"),Wc:a("convert_false_to"),Xc:a("convert_null_to"),Yc:a("convert_true_to"),Zc:a("convert_undefined_to"),qa:a("function"),ye:a("instance_name"),ze:a("live_only"),Ae:a("malware_disabled"),Cg:a("original_vendor_template_id"),Be:a("once_per_event"),md:a("once_per_load"),nd:a("setup_tags"),Ce:a("tag_id"),od:a("teardown_tags")}}();var Dc={},Ec=function(a){Dc.GTM=Dc.GTM||[];Dc.GTM[a]=!0};
var Fc=function(){return"&tc="+Ob.filter(function(a){return a}).length},Oc=function(){Gc&&(x.clearTimeout(Gc),Gc=void 0);void 0===Hc||Ic[Hc]&&!Jc||(Kc[Hc]||Lc.Pf()||0>=Mc--?(Ec(1),Kc[Hc]=!0):(Lc.gg(),Sa(Nc()),Ic[Hc]=!0,Jc=""))},Nc=function(){var a=Hc;if(void 0===a)return"";for(var b,c=[],d=Dc.GTM||[],e=0;e<d.length;e++)d[e]&&(c[Math.floor(e/6)]^=1<<e%6);for(var g=0;g<c.length;g++)c[g]="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_".charAt(c[g]||0);b=c.join("");return[Pc,Ic[a]?"":
"&es=1",Qc[a],b?"&u="+b:"",Fc(),Jc,"&z=0"].join("")},Rc=function(){return[vc,"&v=3&t=t","&pid="+sa(),"&rv="+H.sb].join("")},Sc="0.005000">Math.random(),Pc=Rc(),Tc=function(){Pc=Rc()},Ic={},Jc="",Hc=void 0,Qc={},Kc={},Gc=void 0,Lc=function(a,b){var c=0,d=0;return{Pf:function(){if(c<a)return!1;ya()-d>=b&&(c=0);return c>=a},gg:function(){ya()-d>=b&&(c=0);c++;d=ya()}}}(2,1E3),Mc=1E3,Uc=function(a,b){if(Sc&&!Kc[a]&&Hc!==a){Oc();Hc=a;Jc="";var c;c=0===b.indexOf("gtm.")?encodeURIComponent(b):
"*";Qc[a]="&e="+c+"&eid="+a;Gc||(Gc=x.setTimeout(Oc,500))}},Vc=function(a,b,c){if(Sc&&!Kc[a]&&b){a!==Hc&&(Oc(),Hc=a);var d=c+String(b[P.qa]||"").replace(/_/g,"");Jc=Jc?Jc+"."+d:"&tr="+d;Gc||(Gc=x.setTimeout(Oc,500));2022<=Nc().length&&Oc()}};var Wc=new za,Xc={},Yc={},bd={set:function(a,b){t(Zc(a,b),Xc);$c()},get:function(a){return ad(a,2)},reset:function(){Wc=new za;Xc={};$c()}},ad=function(a,b){if(2!=b){var c=Wc.get(a);if(Sc){var d=cd(a);c!==d&&Ec(5)}return c}return cd(a)},cd=function(a,b,c){var d=a.split("."),e=!1,g=void 0;
return e?g:ed(d)},ed=function(a){for(var b=Xc,c=0;c<a.length;c++){if(null===b)return!1;if(void 0===b)break;b=b[a[c]]}return b};
var hd=function(a,b){Yc.hasOwnProperty(a)||(Wc.set(a,b),t(Zc(a,b),Xc),$c())},Zc=function(a,b){for(var c={},d=c,e=a.split("."),g=0;g<e.length-1;g++)d=d[e[g]]={};d[e[e.length-1]]=b;return c},$c=function(a){ta(Yc,function(b,c){Wc.set(b,c);t(Zc(b,void 0),Xc);t(Zc(b,c),Xc);a&&delete Yc[b]})};var id=new RegExp(/^(.*\.)?(google|youtube|blogger|withgoogle)(\.com?)?(\.[a-z]{2})?\.?$/),jd={cl:["ecl"],customPixels:["nonGooglePixels"],ecl:["cl"],html:["customScripts","customPixels","nonGooglePixels","nonGoogleScripts","nonGoogleIframes"],customScripts:["html","customPixels","nonGooglePixels","nonGoogleScripts","nonGoogleIframes"],nonGooglePixels:[],nonGoogleScripts:["nonGooglePixels"],nonGoogleIframes:["nonGooglePixels"]},kd={cl:["ecl"],customPixels:["customScripts","html"],ecl:["cl"],html:["customScripts"],
customScripts:["html"],nonGooglePixels:["customPixels","customScripts","html","nonGoogleScripts","nonGoogleIframes"],nonGoogleScripts:["customScripts","html"],nonGoogleIframes:["customScripts","html","nonGoogleScripts"]};
var md=function(a){var b=ad("gtm.whitelist");b&&Ec(9);var c=b&&Ga(wa(b),jd),d=ad("gtm.blacklist");d||(d=ad("tagTypeBlacklist"))&&Ec(3);d?Ec(8):d=[];
ld()&&(d=wa(d),d.push("nonGooglePixels","nonGoogleScripts"));0<=n(wa(d),"google")&&Ec(2);var e=d&&Ga(wa(d),kd),g={};return function(h){var k=h&&h[P.qa];if(!k||"string"!=typeof k)return!0;k=k.replace(/^_*/,"");if(void 0!==g[k])return g[k];var l=xc[k]||[],m=a(k);if(b){var p;if(p=m)a:{if(0>n(c,k))if(l&&0<l.length)for(var q=0;q<l.length;q++){if(0>n(c,l[q])){Ec(11);p=!1;break a}}else{p=!1;break a}p=!0}m=p}var r=!1;if(d){var w=0<=n(e,k);
if(w)r=w;else{var v;b:{for(var u=l||[],z=new za,E=0;E<e.length;E++)z.set(e[E],!0);for(var B=0;B<u.length;B++)if(z.get(u[B])){v=!0;break b}v=!1}var G=v;G&&Ec(10);r=G}}return g[k]=!m||r}},ld=function(){return id.test(x.location&&x.location.hostname)};var od=function(a){return function(b,c){if(oa(c))a(b,c);else{b instanceof nd||(b=new nd(b));var d=b;c&&d.jc.push(c);throw d;}}},nd=function(a){this.Tf=a;this.jc=[]};la(nd,Error);var pd={ff:function(a,b){b[P.Vc]&&"string"===typeof a&&(a=1==b[P.Vc]?a.toLowerCase():a.toUpperCase());b.hasOwnProperty(P.Xc)&&null===a&&(a=b[P.Xc]);b.hasOwnProperty(P.Zc)&&void 0===a&&(a=b[P.Zc]);b.hasOwnProperty(P.Yc)&&!0===a&&(a=b[P.Yc]);b.hasOwnProperty(P.Wc)&&!1===a&&(a=b[P.Wc]);return a}};var qd={active:!0,isWhitelisted:function(){return!0}},rd=function(a){var b=K.zones;!b&&a&&(b=K.zones=a());return b};var sd=!1,td=0,ud=[];function vd(a){if(!sd){var b=y.createEventObject,c="complete"==y.readyState,d="interactive"==y.readyState;if(!a||"readystatechange"!=a.type||c||!b&&d){sd=!0;for(var e=0;e<ud.length;e++)C(ud[e])}ud.push=function(){for(var a=0;a<arguments.length;a++)C(arguments[a]);return 0}}}function wd(){if(!sd&&140>td){td++;try{y.documentElement.doScroll("left"),vd()}catch(a){x.setTimeout(wd,50)}}}var xd=function(a){sd?a():ud.push(a)};var yd=function(){function a(a){return!oa(a)||0>a?0:a}if(!K._li&&x.performance&&x.performance.timing){var b=x.performance.timing.navigationStart,c=oa(bd.get("gtm.start"))?bd.get("gtm.start"):0;K._li={cst:a(c-b),cbt:a(tc-b)}}};var Cd=!1,Dd=function(){return x.GoogleAnalyticsObject&&x[x.GoogleAnalyticsObject]},Ed=!1;var Fd=function(a){x.GoogleAnalyticsObject||(x.GoogleAnalyticsObject=a||"ga");var b=x.GoogleAnalyticsObject;if(!x[b]){var c=function(){c.q=c.q||[];c.q.push(arguments)};c.l=Number(new Date);x[b]=c}yd();return x[b]},Gd=function(a,b,c,d){b=String(b).replace(/\s+/g,"").split(",");var e=Dd();e(a+"require","linker");e(a+"linker:autoLink",b,c,d)};
var Id=function(){},Hd=function(){return x.GoogleAnalyticsObject||"ga"},Jd=!1;var Qd=function(a){};
function Pd(a,b){a.containerId=H.m;var c={type:"GENERIC",value:a};b.length&&(c.trace=b);return c};function Rd(a,b,c,d,e){var g=Ob[a],h=Sd(a,b,c,d,e);if(!h)return null;var k=Wb(g[P.nd],d.da,[],ma);if(k&&k.length){var l=k[0];h=Rd(l.index,b,{I:h,O:1===l.wd?c.terminate:h,terminate:c.terminate},d,e)}return h}
function Sd(a,b,c,d,e){function g(){if(h[P.Ae])l();else{var b=Xb(h,d.da,[],od(function(a){Ec(6);Qd(a)})),c=!1;b.vtp_gtmOnSuccess=function(){if(!c){c=!0;Vc(d.id,Ob[a],"5");k()}};b.vtp_gtmOnFailure=function(){if(!c){c=!0;Vc(d.id,Ob[a],"6");l()}};b.vtp_gtmTagId=h.tag_id;Vc(d.id,h,"1");var e=
!1,g=function(a,b){if(!e){a instanceof nd||(a=new nd(a));var g=a;b&&g.jc.push(b);throw g;}Qd(a);Vc(d.id,h,"7");c||(c=!0,l())};try{Vb(b,g)}catch(M){try{e=!0,g(M)}catch(F){}}}}
var h=Ob[a],k=c.I,l=c.O,m=c.terminate;if(d.da(h))return null;var p=Wb(h[P.od],d.da,[],ma);if(p&&p.length){var q=p[0],r=Rd(q.index,b,{I:k,O:l,terminate:m},d,e);if(!r)return null;k=r;l=2===q.wd?m:r}if(h[P.md]||h[P.Be]){var w=h[P.md]?Pb:b,v=k,u=l;if(!w[a]){g=Ca(g);var z=Td(a,w,g);k=z.I;l=z.O}return function(){w[a](v,u)}}return g}function Td(a,b,c){var d=[],e=[];b[a]=Ud(d,e,c);return{I:function(){b[a]=Vd;for(var c=0;c<d.length;c++)d[c]()},O:function(){b[a]=Wd;for(var c=0;c<e.length;c++)e[c]()}}}
function Ud(a,b,c){return function(d,e){a.push(d);b.push(e);c()}}function Vd(a){a()}function Wd(a,b){b()};function Xd(a){var b=0,c=0,d=!1;return{add:function(){c++;return Ca(function(){b++;d&&b>=c&&a()})},Qe:function(){d=!0;b>=c&&a()}}}var $d=function(a){for(var b=Xd(a.callback),c=[],d=[],e=0;e<Ob.length;e++)if(a.Va[e]){var g=Ob[e];var h=b.add();try{var k=Rd(e,c,{I:h,O:h,terminate:h},a,e);k?d.push({Wd:e,b:Yb(g),uf:k}):(Yd(e,a),h())}catch(m){h()}}b.Qe();d.sort(Zd);for(var l=0;l<d.length;l++)d[l].uf();return 0<d.length};
function Zd(a,b){var c,d=b.b,e=a.b;c=d>e?1:d<e?-1:0;var g;if(0!==c)g=c;else{var h=a.Wd,k=b.Wd;g=h>k?1:h<k?-1:0}return g}function Yd(a,b){if(!Sc)return;var c=function(a){var d=b.da(Ob[a])?"3":"4",g=Wb(Ob[a][P.nd],b.da,[],ma);g&&g.length&&c(g[0].index);Vc(b.id,Ob[a],d);var h=Wb(Ob[a][P.od],b.da,[],ma);h&&h.length&&c(h[0].index)};c(a);}
var ae=!1,be=function(a,b,c,d){if("gtm.js"==b){if(ae)return!1;ae=!0}Uc(a,b);var e=md(c),g={id:a,name:b,callback:d||ma,da:e,Va:[]};g.Va=cc(e,od(function(a){Qd(a)}));var h=$d(g);"gtm.js"!==b&&"gtm.sync"!==b||Id();if(!h)return h;for(var k={__cl:!0,__ecl:!0,__evl:!0,__fsl:!0,__hl:!0,__jel:!0,__lcl:!0,__sdl:!0,__tl:!0,__ytl:!0},l=0;l<g.Va.length;l++)if(g.Va[l]){var m=
Ob[l];if(m&&!k[m[P.qa]])return!0}return!1};var Q={Pb:"event_callback",Rb:"event_timeout"};var de={};var ee=/[A-Z]+/,fe=/\s/,ge=function(a){if(f(a)&&(a=xa(a),!fe.test(a))){var b=a.indexOf("-");if(!(0>b)){var c=a.substring(0,b);if(ee.test(c)){for(var d=a.substring(b+1).split("/"),e=0;e<d.length;e++)if(!d[e])return;return{id:a,prefix:c,containerId:c+"-"+d[0],ca:d}}}}},ie=function(a){for(var b={},c=0;c<a.length;++c){var d=ge(a[c]);d&&(b[d.id]=d)}he(b);var e=[];ta(b,function(a,b){e.push(b)});return e};
function he(a){var b=[],c;for(c in a)if(a.hasOwnProperty(c)){var d=a[c];"AW"===d.prefix&&d.ca[1]&&b.push(d.containerId)}for(var e=0;e<b.length;++e)delete a[b[e]]};var je=null,ke={},le={},me,ne=function(a,b){var c={event:a};b&&(c.eventModel=t(b),b[Q.Pb]&&(c.eventCallback=b[Q.Pb]),b[Q.Rb]&&(c.eventTimeout=b[Q.Rb]));return c};
var se={config:function(a){},event:function(a){var b=a[1];if(f(b)&&!(3<a.length)){var c;if(2<a.length){if(!Ka(a[2]))return;
c=a[2]}var d=ne(b,c);return d}},js:function(a){if(2==a.length&&a[1].getTime)return{event:"gtm.js","gtm.start":a[1].getTime()}},policy:function(a){if(3===a.length){var b=a[1],c=a[2];de[b]||(de[b]=[]);de[b].push(c)}},set:function(a){var b;2==a.length&&Ka(a[1])?b=t(a[1]):3==a.length&&f(a[1])&&(b={},b[a[1]]=a[2]);if(b)return b.eventModel=t(b),b.event="gtag.set",b._clear=!0,b}},te={policy:!0};var ue=function(){return!1};var ze=function(a){this.vg=a};ze.prototype.Af=function(){return this.vg};var Ae=function(a){return!a||"object"!==Ia(a)||Ka(a)?!1:"getUntrustedUpdateValue"in a};ze.prototype.getUntrustedUpdateValue=ze.prototype.Af;var Be=!1,Ce=[];function De(){if(!Be){Be=!0;for(var a=0;a<Ce.length;a++)C(Ce[a])}}var Ee=function(a){Be?C(a):Ce.push(a)};var Fe=[],Ge=!1;function He(a){var b=a.eventCallback,c=Ca(function(){na(b)&&C(function(){b(H.m)})}),d=a.eventTimeout;d&&x.setTimeout(c,Number(d));return c}
var Ie=function(a){return x["dataLayer"].push(a)},Ke=function(a){var b=a._clear;ta(a,function(a,c){"_clear"!==a&&(b&&hd(a,void 0),hd(a,c))});var c=a.event;if(!c)return!1;var d=a["gtm.uniqueEventId"];d||(d=yc(),a["gtm.uniqueEventId"]=d,hd("gtm.uniqueEventId",d));uc=c;var e=Je(a);uc=null;if(!sc){sc=a["gtm.start"];}return e};
function Je(a){var b=a.event,c=a["gtm.uniqueEventId"],d,e=K.zones;d=e?e.checkState(H.m,c):qd;if(!d.active)return!1;var g=He(a);return be(c,b,d.isWhitelisted,g)?!0:!1}
var Le=function(){for(var a=!1;!Ge&&0<Fe.length;){Ge=!0;delete Xc.eventModel;$c();var b=Fe.shift();if(null!=b){var c=Ae(b);if(c){var d=b;b=Ae(d)?d.getUntrustedUpdateValue():void 0;for(var e=["gtm.whitelist","gtm.blacklist","tagTypeBlacklist"],g=0;g<e.length;g++){var h=e[g],k=ad(h,1);if(qa(k)||Ka(k))k=t(k);Yc[h]=k}}try{if(na(b))try{b.call(bd)}catch(u){}else if(qa(b)){var l=b;if(f(l[0])){var m=
l[0].split("."),p=m.pop(),q=l.slice(1),r=ad(m.join("."),2);if(void 0!==r&&null!==r)try{r[p].apply(r,q)}catch(u){}}}else{var w=b;if(w&&("[object Arguments]"==Object.prototype.toString.call(w)||Object.prototype.hasOwnProperty.call(w,"callee"))){a:{if(b.length&&f(b[0])){var v=se[b[0]];if(v&&(!c||!te[b[0]])){b=v(b);break a}}b=void 0}if(!b){Ge=!1;continue}}a=Ke(b)||a}}finally{c&&$c(!0)}}Ge=!1}
return!a},Me=function(){var a=Le();try{var b=H.m,c=x["dataLayer"].hide;if(c&&void 0!==c[b]&&c.end){c[b]=!1;var d=!0,e;for(e in c)if(c.hasOwnProperty(e)&&!0===c[e]){d=!1;break}d&&(c.end(),c.end=null)}}catch(g){}return a},Ne=function(){var a=Na("dataLayer",[]),b=Na("google_tag_manager",{});b=b["dataLayer"]=b["dataLayer"]||{};xd(function(){b.gtmDom||(b.gtmDom=!0,a.push({event:"gtm.dom"}))});Ee(function(){b.gtmLoad||(b.gtmLoad=!0,a.push({event:"gtm.load"}))});var c=a.push;a.push=function(){var b;
if(0<K.SANDBOXED_JS_SEMAPHORE){b=[];for(var e=0;e<arguments.length;e++)b[e]=new ze(arguments[e])}else b=[].slice.call(arguments,0);var g=c.apply(a,b);Fe.push.apply(Fe,b);if(300<this.length)for(Ec(4);300<this.length;)this.shift();var h="boolean"!==typeof g||g;return Le()&&h};Fe.push.apply(Fe,a.slice(0));C(Me)};var Pe=function(a){return Oe?y.querySelectorAll(a):null},Qe=function(a,b){if(!Oe)return null;if(Element.prototype.closest)try{return a.closest(b)}catch(e){return null}var c=Element.prototype.matches||Element.prototype.webkitMatchesSelector||Element.prototype.mozMatchesSelector||Element.prototype.msMatchesSelector||Element.prototype.oMatchesSelector,d=a;if(!y.documentElement.contains(d))return null;do{try{if(c.call(d,b))return d}catch(e){break}d=d.parentElement||d.parentNode}while(null!==d&&1===d.nodeType);
return null},Re=!1;if(y.querySelectorAll)try{var Se=y.querySelectorAll(":root");Se&&1==Se.length&&Se[0]==y.documentElement&&(Re=!0)}catch(a){}var Oe=Re;var Te;var pf={};pf.ob=new String("undefined");
var qf=function(a){this.resolve=function(b){for(var c=[],d=0;d<a.length;d++)c.push(a[d]===pf.ob?b:a[d]);return c.join("")}};qf.prototype.toString=function(){return this.resolve("undefined")};qf.prototype.valueOf=qf.prototype.toString;pf.De=qf;pf.$b={};pf.kf=function(a){return new qf(a)};var rf={};pf.hg=function(a,b){var c=yc();rf[c]=[a,b];return c};pf.td=function(a){var b=a?0:1;return function(a){var c=rf[a];if(c&&"function"===typeof c[b])c[b]();rf[a]=void 0}};pf.Nf=function(a){for(var b=!1,c=!1,
d=2;d<a.length;d++)b=b||8===a[d],c=c||16===a[d];return b&&c};pf.Yf=function(a){if(a===pf.ob)return a;var b=yc();pf.$b[b]=a;return'google_tag_manager["'+H.m+'"].macro('+b+")"};pf.Rf=function(a,b,c){a instanceof pf.De&&(a=a.resolve(pf.hg(b,c)),b=ma);return{oc:a,I:b}};var sf=function(a,b,c){var d={event:b,"gtm.element":a,"gtm.elementClasses":a.className,"gtm.elementId":a["for"]||Ua(a,"id")||"","gtm.elementTarget":a.formTarget||a.target||""};c&&(d["gtm.triggers"]=c.join(","));d["gtm.elementUrl"]=(a.attributes&&a.attributes.formaction?a.formAction:"")||a.action||a.href||a.src||a.code||a.codebase||"";return d},tf=function(a){K.hasOwnProperty("autoEventsSettings")||(K.autoEventsSettings={});var b=K.autoEventsSettings;b.hasOwnProperty(a)||(b[a]={});return b[a]},uf=
function(a,b,c,d){var e=tf(a),g=Ba(e,b,d);e[b]=c(g)},vf=function(a,b,c){var d=tf(a);return Ba(d,b,c)};var wf=function(){for(var a=La.userAgent+(y.cookie||"")+(y.referrer||""),b=a.length,c=x.history.length;0<c;)a+=c--^b++;var d=1,e,g,h;if(a)for(d=0,g=a.length-1;0<=g;g--)h=a.charCodeAt(g),d=(d<<6&268435455)+h+(h<<14),e=d&266338304,d=0!=e?d^e>>21:d;return[Math.round(2147483647*Math.random())^d&2147483647,Math.round(ya()/1E3)].join(".")},zf=function(a,b,c,d){var e=xf(b);return jb(a,e,yf(c),d)},xf=function(a){if(!a)return 1;a=0===a.indexOf(".")?a.substr(1):a;return a.split(".").length},yf=function(a){if(!a||
"/"===a)return 1;"/"!==a[0]&&(a="/"+a);"/"!==a[a.length-1]&&(a+="/");return a.split("/").length-1};function Af(a,b){var c=""+xf(a),d=yf(b);1<d&&(c+="-"+d);return c};var Bf=["1"],Cf={},Gf=function(a,b,c,d){var e=Df(a);Cf[e]||Ef(e,b,c)||(Ff(e,wf(),b,c,d),Ef(e,b,c))};function Ff(a,b,c,d,e){var g;g=["1",Af(d,c),b].join(".");nb(a,g,c,d,0==e?void 0:new Date(ya()+1E3*(void 0==e?7776E3:e)))}function Ef(a,b,c){var d=zf(a,b,c,Bf);d&&(Cf[a]=d);return d}function Df(a){return(a||"_gcl")+"_au"};var Hf=function(){for(var a=[],b=y.cookie.split(";"),c=/^\s*_gac_(UA-\d+-\d+)=\s*(.+?)\s*$/,d=0;d<b.length;d++){var e=b[d].match(c);e&&a.push({Nc:e[1],value:e[2]})}var g={};if(!a||!a.length)return g;for(var h=0;h<a.length;h++){var k=a[h].value.split(".");"1"==k[0]&&3==k.length&&k[1]&&(g[a[h].Nc]||(g[a[h].Nc]=[]),g[a[h].Nc].push({timestamp:k[1],xf:k[2]}))}return g};function If(){for(var a=Jf,b={},c=0;c<a.length;++c)b[a[c]]=c;return b}function Kf(){var a="ABCDEFGHIJKLMNOPQRSTUVWXYZ";a+=a.toLowerCase()+"0123456789-_";return a+"."}
var Jf,Lf,Mf=function(a){Jf=Jf||Kf();Lf=Lf||If();for(var b=[],c=0;c<a.length;c+=3){var d=c+1<a.length,e=c+2<a.length,g=a.charCodeAt(c),h=d?a.charCodeAt(c+1):0,k=e?a.charCodeAt(c+2):0,l=g>>2,m=(g&3)<<4|h>>4,p=(h&15)<<2|k>>6,q=k&63;e||(q=64,d||(p=64));b.push(Jf[l],Jf[m],Jf[p],Jf[q])}return b.join("")},Nf=function(a){function b(b){for(;d<a.length;){var c=a.charAt(d++),e=Lf[c];if(null!=e)return e;if(!/^[\s\xa0]*$/.test(c))throw Error("Unknown base64 encoding at char: "+c);}return b}Jf=Jf||Kf();Lf=Lf||
If();for(var c="",d=0;;){var e=b(-1),g=b(0),h=b(64),k=b(64);if(64===k&&-1===e)return c;c+=String.fromCharCode(e<<2|g>>4);64!=h&&(c+=String.fromCharCode(g<<4&240|h>>2),64!=k&&(c+=String.fromCharCode(h<<6&192|k)))}};var Of;function Pf(a,b){if(!a||b===y.location.hostname)return!1;for(var c=0;c<a.length;c++)if(a[c]instanceof RegExp){if(a[c].test(b))return!0}else if(0<=b.indexOf(a[c]))return!0;return!1}var Qf=function(){var a=Na("google_tag_data",{}),b=a.gl;b&&b.decorators||(b={decorators:[]},a.gl=b);return b};var Rf=/(.*?)\*(.*?)\*(.*)/,Sf=/^https?:\/\/([^\/]*?)\.?cdn\.ampproject\.org\/?(.*)/,Tf=/^(?:www\.|m\.|amp\.)+/,Uf=/([^?#]+)(\?[^#]*)?(#.*)?/,Vf=/(.*?)(^|&)_gl=([^&]*)&?(.*)/,Xf=function(a){var b=[],c;for(c in a)if(a.hasOwnProperty(c)){var d=a[c];void 0!==d&&d===d&&null!==d&&"[object Object]"!==d.toString()&&(b.push(c),b.push(Mf(String(d))))}var e=b.join("*");return["1",Wf(e),e].join("*")},Wf=function(a,b){var c=[window.navigator.userAgent,(new Date).getTimezoneOffset(),window.navigator.userLanguage||
window.navigator.language,Math.floor((new Date).getTime()/60/1E3)-(void 0===b?0:b),a].join("*"),d;if(!(d=Of)){for(var e=Array(256),g=0;256>g;g++){for(var h=g,k=0;8>k;k++)h=h&1?h>>>1^3988292384:h>>>1;e[g]=h}d=e}Of=d;for(var l=4294967295,m=0;m<c.length;m++)l=l>>>8^Of[(l^c.charCodeAt(m))&255];return((l^-1)>>>0).toString(36)},Zf=function(){return function(a){var b=fb(x.location.href),c=b.search.replace("?",""),d=cb(c,"_gl",!0)||"";a.query=Yf(d)||{};var e=D(b,"fragment").match(Vf);a.fragment=Yf(e&&e[3]||
"")||{}}},Yf=function(a){var b;b=void 0===b?3:b;try{if(a){var c;a:{for(var d=a,e=0;3>e;++e){var g=Rf.exec(d);if(g){c=g;break a}d=decodeURIComponent(d)}c=void 0}var h=c;if(h&&"1"===h[1]){var k=h[3],l;a:{for(var m=h[2],p=0;p<b;++p)if(m===Wf(k,p)){l=!0;break a}l=!1}if(l){for(var q={},r=k?k.split("*"):[],w=0;w<r.length;w+=2)q[r[w]]=Nf(r[w+1]);return q}}}}catch(v){}};
function $f(a,b,c){function d(a){var b=a,c=Vf.exec(b),d=b;if(c){var e=c[2],g=c[4];d=c[1];g&&(d=d+e+g)}a=d;var h=a.charAt(a.length-1);a&&"&"!==h&&(a+="&");return a+l}c=void 0===c?!1:c;var e=Uf.exec(b);if(!e)return"";var g=e[1],h=e[2]||"",k=e[3]||"",l="_gl="+a;c?k="#"+d(k.substring(1)):h="?"+d(h.substring(1));return""+g+h+k}
function ag(a,b,c){for(var d={},e={},g=Qf().decorators,h=0;h<g.length;++h){var k=g[h];(!c||k.forms)&&Pf(k.domains,b)&&(k.fragment?Da(e,k.callback()):Da(d,k.callback()))}if(Fa(d)){var l=Xf(d);if(c){if(a&&a.action){var m=(a.method||"").toLowerCase();if("get"===m){for(var p=a.childNodes||[],q=!1,r=0;r<p.length;r++){var w=p[r];if("_gl"===w.name){w.setAttribute("value",l);q=!0;break}}if(!q){var v=y.createElement("input");v.setAttribute("type","hidden");v.setAttribute("name","_gl");v.setAttribute("value",
l);a.appendChild(v)}}else if("post"===m){var u=$f(l,a.action);Za.test(u)&&(a.action=u)}}}else bg(l,a,!1)}if(!c&&Fa(e)){var z=Xf(e);bg(z,a,!0)}}function bg(a,b,c){if(b.href){var d=$f(a,b.href,void 0===c?!1:c);Za.test(d)&&(b.href=d)}}
var cg=function(a){try{var b;a:{for(var c=a.target||a.srcElement||{},d=100;c&&0<d;){if(c.href&&c.nodeName.match(/^a(?:rea)?$/i)){b=c;break a}c=c.parentNode;d--}b=null}var e=b;if(e){var g=e.protocol;"http:"!==g&&"https:"!==g||ag(e,e.hostname,!1)}}catch(h){}},dg=function(a){try{var b=a.target||a.srcElement||{};if(b.action){var c=D(fb(b.action),"host");ag(b,c,!0)}}catch(d){}},eg=function(a,b,c,d){var e=Qf();e.init||(A(y,"mousedown",cg),A(y,"keyup",cg),A(y,"submit",dg),e.init=!0);var g={callback:a,domains:b,
fragment:"fragment"===c,forms:!!d};Qf().decorators.push(g)},fg=function(){var a=y.location.hostname,b=Sf.exec(y.referrer);if(!b)return!1;var c=b[2],d=b[1],e="";if(c){var g=c.split("/"),h=g[1];e="s"===h?decodeURIComponent(g[2]):decodeURIComponent(h)}else if(d){if(0===d.indexOf("xn--"))return!1;e=d.replace(/-/g,".").replace(/\.\./g,"-")}return a.replace(Tf,"")===e.replace(Tf,"")},gg=function(a,b){return!1===a?!1:a||b||fg()};var hg=/^\w+$/,ig=/^[\w-]+$/,jg=/^~?[\w-]+$/,kg={aw:"_aw",dc:"_dc",gf:"_gf",ha:"_ha"};function lg(a){return a&&"string"==typeof a&&a.match(hg)?a:"_gcl"}var ng=function(){var a=fb(x.location.href),b=D(a,"query",!1,void 0,"gclid"),c=D(a,"query",!1,void 0,"gclsrc"),d=D(a,"query",!1,void 0,"dclid");if(!b||!c){var e=a.hash.replace("#","");b=b||cb(e,"gclid",void 0);c=c||cb(e,"gclsrc",void 0)}return mg(b,c,d)};
function mg(a,b,c){var d={},e=function(a,b){d[b]||(d[b]=[]);d[b].push(a)};if(void 0!==a&&a.match(ig))switch(b){case void 0:e(a,"aw");break;case "aw.ds":e(a,"aw");e(a,"dc");break;case "ds":e(a,"dc");break;case "gf":e(a,"gf");break;case "ha":e(a,"ha")}c&&e(c,"dc");return d}
function og(a,b,c){function d(a,b){var c=pg(a,e);c&&nb(c,b,h,g,l,!0)}b=b||{};var e=lg(b.prefix),g=b.domain||"auto",h=b.path||"/",k=void 0==b.Jd?7776E3:b.Jd;c=c||ya();var l=0==k?void 0:new Date(c+1E3*k),m=Math.round(c/1E3),p=function(a){return["GCL",m,a].join(".")};a.aw&&(!0===b.bh?d("aw",p("~"+a.aw[0])):d("aw",p(a.aw[0])));a.dc&&d("dc",p(a.dc[0]));a.gf&&d("gf",p(a.gf[0]));a.ha&&d("ha",p(a.ha[0]))}
var pg=function(a,b){var c=kg[a];if(void 0!==c)return b+c},qg=function(a){var b=a.split(".");return 3!==b.length||"GCL"!==b[0]?0:1E3*(Number(b[1])||0)};function rg(a){var b=a.split(".");if(3==b.length&&"GCL"==b[0]&&b[1])return b[2]}
var sg=function(a,b,c,d,e){if(qa(b)){var g=lg(e);eg(function(){for(var b={},c=0;c<a.length;++c){var d=pg(a[c],g);if(d){var e=gb(d,y.cookie);e.length&&(b[d]=e.sort()[e.length-1])}}return b},b,c,d)}},tg=function(a){return a.filter(function(a){return jg.test(a)})},ug=function(a,b){for(var c=lg(b&&b.prefix),d={},e=0;e<a.length;e++)kg[a[e]]&&(d[a[e]]=kg[a[e]]);ta(d,function(a,d){var e=gb(c+d,y.cookie);if(e.length){var g=e[0],h=qg(g),p={};p[a]=[rg(g)];og(p,b,h)}})};var vg=/^\d+\.fls\.doubleclick\.net$/;function wg(a){var b=fb(x.location.href),c=D(b,"host",!1);if(c&&c.match(vg)){var d=D(b,"path").split(a+"=");if(1<d.length)return d[1].split(";")[0].split("?")[0]}}
function xg(a,b){if("aw"==a||"dc"==a){var c=wg("gcl"+a);if(c)return c.split(".")}var d=lg(b);if("_gcl"==d){var e;e=ng()[a]||[];if(0<e.length)return e}var g=pg(a,d),h;if(g){var k=[];if(y.cookie){var l=gb(g,y.cookie);if(l&&0!=l.length){for(var m=0;m<l.length;m++){var p=rg(l[m]);p&&-1===n(k,p)&&k.push(p)}h=tg(k)}else h=k}else h=k}else h=[];return h}
var yg=function(){var a=wg("gac");if(a)return decodeURIComponent(a);var b=Hf(),c=[];ta(b,function(a,b){for(var d=[],e=0;e<b.length;e++)d.push(b[e].xf);d=tg(d);d.length&&c.push(a+":"+d.join(","))});return c.join(";")},zg=function(a,b,c,d,e){Gf(b,c,d,e);var g=Cf[Df(b)],h=ng().dc||[],k=!1;if(g&&0<h.length){var l=K.joined_au=K.joined_au||{},m=b||"_gcl";if(!l[m])for(var p=0;p<h.length;p++){var q="https://adservice.google.com/ddm/regclk",r=q=q+"?gclid="+h[p]+"&auiddc="+g;La.sendBeacon&&La.sendBeacon(r)||Sa(r);k=l[m]=
!0}}k|=a;if(k&&g){var w=Df(b),v=Cf[w];v&&Ff(w,v,c,d,e)}};var Ag;if(3===H.sb.length)Ag="g";else{var Bg="G";Ag=Bg}
var Cg={"":"n",UA:"u",AW:"a",DC:"d",G:"e",GF:"f",HA:"h",GTM:Ag},Dg=function(a){var b=H.m.split("-"),c=b[0].toUpperCase(),d=Cg[c]||"i",e=a&&"GTM"===c?b[1]:"",g;if(3===H.sb.length){var h=void 0;g="2"+(h||"w")}else g="";return g+d+H.sb+e};var Kg=!!x.MutationObserver,Lg=void 0,Mg=function(a){if(!Lg){var b=function(){var a=y.body;if(a)if(Kg)(new MutationObserver(function(){for(var a=0;a<Lg.length;a++)C(Lg[a])})).observe(a,{childList:!0,subtree:!0});else{var b=!1;A(a,"DOMNodeInserted",function(){b||(b=!0,C(function(){b=!1;for(var a=0;a<Lg.length;a++)C(Lg[a])}))})}};Lg=[];y.body?b():C(b)}Lg.push(a)};var dh=x.clearTimeout,eh=x.setTimeout,T=function(a,b,c){if(ue()){b&&C(b)}else return Pa(a,b,c)},fh=function(){return new Date},U=function(){return x.location.href},gh=function(a){return D(fb(a),"fragment")},hh=function(a){return eb(fb(a))},V=function(a,b){return ad(a,b||2)},ih=function(a,b,c){b&&(a.eventCallback=b,c&&(a.eventTimeout=c));return Ie(a)},jh=function(a,b){x[a]=b},W=function(a,b,c){b&&(void 0===x[a]||c&&
!x[a])&&(x[a]=b);return x[a]},kh=function(a,b,c){return gb(a,b,void 0===c?!0:!!c)},lh=function(a,b,c,d){var e={prefix:a,path:b,domain:c,Jd:d},g=ng();og(g,e);ug(["aw"],e);ug(["dc"],e);},mh=function(a,b,c,d,e){var g=Zf(),h=Qf();h.data||(h.data=
{query:{},fragment:{}},g(h.data));var k={},l=h.data;l&&(Da(k,l.query),Da(k,l.fragment));for(var m=lg(b),p=0;p<a.length;++p){var q=a[p];if(void 0!==kg[q]){var r=pg(q,m),w=k[r];if(w){var v=Math.min(qg(w),ya()),u;b:{for(var z=v,E=gb(r,y.cookie),B=0;B<E.length;++B)if(qg(E[B])>z){u=!0;break b}u=!1}u||nb(r,w,c,d,0==e?void 0:new Date(v+1E3*(null==e?7776E3:e)),!0)}}}var G={prefix:b,path:c,domain:d};og(mg(k.gclid,k.gclsrc),G);},nh=function(a,b,c,d,e){
sg(a,b,c,d,e);},oh=function(a,b){if(ue()){b&&C(b)}else Ra(a,b)},ph=function(a){return!!vf(a,"init",!1)},qh=function(a){tf(a).init=!0},rh=function(a,b,c){var d=(void 0===c?0:c)?"www.googletagmanager.com/gtag/js":rc;d+="?id="+encodeURIComponent(a)+"&l=dataLayer";b&&ta(b,function(a,b){b&&(d+="&"+a+"="+encodeURIComponent(b))});T(O("https://","http://",d))};var th=pf.Rf;
var uh=new za;function vh(a,b){function c(a){var b=fb(a),c=D(b,"protocol"),d=D(b,"host",!0),e=D(b,"port"),g=D(b,"path").toLowerCase().replace(/\/$/,"");if(void 0===c||"http"==c&&"80"==e||"https"==c&&"443"==e)c="web",e="default";return[c,d,e,g]}for(var d=c(String(a)),e=c(String(b)),g=0;g<d.length;g++)if(d[g]!==e[g])return!1;return!0}
function wh(a){var b=a.arg0,c=a.arg1;if(a.any_of&&qa(c)){for(var d=0;d<c.length;d++)if(wh({"function":a["function"],arg0:b,arg1:c[d]}))return!0;return!1}switch(a["function"]){case "_cn":return 0<=String(b).indexOf(String(c));case "_css":var e;a:{if(b){var g=["matches","webkitMatchesSelector","mozMatchesSelector","msMatchesSelector","oMatchesSelector"];try{for(var h=0;h<g.length;h++)if(b[g[h]]){e=b[g[h]](c);break a}}catch(u){}}e=!1}return e;case "_ew":var k,l;k=String(b);l=String(c);var m=k.length-
l.length;return 0<=m&&k.indexOf(l,m)==m;case "_eq":return String(b)==String(c);case "_ge":return Number(b)>=Number(c);case "_gt":return Number(b)>Number(c);case "_lc":var p;p=String(b).split(",");return 0<=n(p,String(c));case "_le":return Number(b)<=Number(c);case "_lt":return Number(b)<Number(c);case "_re":var q;var r=a.ignore_case?"i":void 0;try{var w=String(c)+r,v=uh.get(w);v||(v=new RegExp(c,r),uh.set(w,v));q=v.test(b)}catch(u){q=!1}return q;case "_sw":return 0==String(b).indexOf(String(c));case "_um":return vh(b,
c)}return!1};var yh=function(a,b){var c=function(){};c.prototype=a.prototype;var d=new c;a.apply(d,Array.prototype.slice.call(arguments,1));return d};var zh={},Ah=encodeURI,X=encodeURIComponent,Bh=Sa;var Ch=function(a,b){if(!a)return!1;var c=D(fb(a),"host");if(!c)return!1;for(var d=0;b&&d<b.length;d++){var e=b[d]&&b[d].toLowerCase();if(e){var g=c.length-e.length;0<g&&"."!=e.charAt(0)&&(g--,e="."+e);if(0<=g&&c.indexOf(e,g)==g)return!0}}return!1};
var Y=function(a,b,c){for(var d={},e=!1,g=0;a&&g<a.length;g++)a[g]&&a[g].hasOwnProperty(b)&&a[g].hasOwnProperty(c)&&(d[a[g][b]]=a[g][c],e=!0);return e?d:null};zh.Of=function(){var a=!1;return a};var li=function(a,b,c,d){this.n=a;this.t=b;this.p=c;this.d=d},mi=function(){this.c=1;this.e=[];this.p=null};function ni(a){var b=K,c=b.gss=b.gss||{};return c[a]=c[a]||new mi}var oi=function(a,b){ni(a).p=b},pi=function(a){var b=ni(a),c=b.p;if(c){var d=b.e,e=[];b.e=[];var g=function(a){for(var b=0;b<a.length;b++)try{var d=a[b];d.d?(d.d=!1,e.push(d)):c(d.n,d.t,d.p)}catch(m){}};g(d);g(e)}};var ri=function(){var a=x.gaGlobal=x.gaGlobal||{};a.hid=a.hid||sa();return a.hid};var Gi=window,Hi=document,Ii=function(a){var b=Gi._gaUserPrefs;if(b&&b.ioo&&b.ioo()||a&&!0===Gi["ga-disable-"+a])return!0;try{var c=Gi.external;if(c&&c._gaUserPrefs&&"oo"==c._gaUserPrefs)return!0}catch(g){}for(var d=gb("AMP_TOKEN",Hi.cookie,!0),e=0;e<d.length;e++)if("$OPT_OUT"==d[e])return!0;return Hi.getElementById("__gaOptOutExtension")?!0:!1};var Oi=function(a,b,c){Ni(a);var d=Math.floor(ya()/1E3);ni(a).e.push(new li(b,d,c,void 0));pi(a)},Qi=function(a,b,c){Ni(a);var d=Math.floor(ya()/1E3);ni(a).e.push(new li(b,d,c,!0))},Ni=function(a){if(1===ni(a).c){ni(a).c=2;var b=encodeURIComponent(a);Pa(("http:"!=x.location.protocol?"https:":"http:")+("//www.googletagmanager.com/gtag/js?id="+b+"&l=dataLayer&cx=c"))}},Si=function(a,b){},Ri=function(a,b){};var Z={a:{}};
Z.a.jsm=["customScripts"],function(){(function(a){Z.__jsm=a;Z.__jsm.g="jsm";Z.__jsm.h=!0;Z.__jsm.b=0})(function(a){if(void 0!==a.vtp_javascript){var b=a.vtp_javascript;try{var c=W("google_tag_manager");return c&&c.e&&c.e(b)}catch(d){}}})}();
Z.a.e=["google"],function(){(function(a){Z.__e=a;Z.__e.g="e";Z.__e.h=!0;Z.__e.b=0})(function(){return uc})}();
Z.a.f=["google"],function(){(function(a){Z.__f=a;Z.__f.g="f";Z.__f.h=!0;Z.__f.b=0})(function(a){var b=V("gtm.referrer",1)||y.referrer;return b?a.vtp_component&&"URL"!=a.vtp_component?D(fb(String(b)),a.vtp_component,a.vtp_stripWww,a.vtp_defaultPages,a.vtp_queryKey):hh(String(b)):String(b)})}();Z.a.k=["google"],function(){(function(a){Z.__k=a;Z.__k.g="k";Z.__k.h=!0;Z.__k.b=0})(function(a){return kh(a.vtp_name,V("gtm.cookie",1),!!a.vtp_decodeCookie)[0]})}();
Z.a.r=["google"],function(){(function(a){Z.__r=a;Z.__r.g="r";Z.__r.h=!0;Z.__r.b=0})(function(a){return sa(a.vtp_min,a.vtp_max)})}();

Z.a.u=["google"],function(){var a=function(a){return{toString:function(){return a}}};(function(a){Z.__u=a;Z.__u.g="u";Z.__u.h=!0;Z.__u.b=0})(function(b){var c;c=(c=b.vtp_customUrlSource?b.vtp_customUrlSource:V("gtm.url",1))||U();var d=b[a("vtp_component")];if(!d||"URL"==d)return hh(String(c));var e=fb(String(c)),g;if("QUERY"==d&&b[a("vtp_multiQueryKeys")])a:{var h=b[a("vtp_queryKey")],k;k=qa(h)?h:String(h||"").replace(/\s+/g,"").split(",");for(var l=0;l<k.length;l++){var m=D(e,"QUERY",void 0,void 0,
k[l]);if(null!=m){g=m;break a}}g=void 0}else g=D(e,d,"HOST"==d?b[a("vtp_stripWww")]:void 0,"PATH"==d?b[a("vtp_defaultPages")]:void 0,"QUERY"==d?b[a("vtp_queryKey")]:void 0);return g})}();Z.a.v=["google"],function(){(function(a){Z.__v=a;Z.__v.g="v";Z.__v.h=!0;Z.__v.b=0})(function(a){var b=a.vtp_name;if(!b||!b.replace)return!1;var c=V(b.replace(/\\\./g,"."),a.vtp_dataLayerVersion||1);return void 0!==c?c:a.vtp_defaultValue})}();

Z.a.ua=["google"],function(){var a,b=function(b){var c={},e={},g={},h={},k={};if(b.vtp_gaSettings){var l=b.vtp_gaSettings;t(Y(l.vtp_fieldsToSet,"fieldName","value"),e);t(Y(l.vtp_contentGroup,"index","group"),g);t(Y(l.vtp_dimension,"index","dimension"),h);t(Y(l.vtp_metric,"index","metric"),k);b.vtp_gaSettings=null;l.vtp_fieldsToSet=void 0;l.vtp_contentGroup=void 0;l.vtp_dimension=void 0;l.vtp_metric=void 0;var m=t(l);b=t(b,m)}t(Y(b.vtp_fieldsToSet,"fieldName","value"),e);t(Y(b.vtp_contentGroup,"index",
"group"),g);t(Y(b.vtp_dimension,"index","dimension"),h);t(Y(b.vtp_metric,"index","metric"),k);var p=Fd(b.vtp_functionName);if(na(p)){var q="",r="";b.vtp_setTrackerName&&"string"==typeof b.vtp_trackerName?""!==b.vtp_trackerName&&(r=b.vtp_trackerName,q=r+"."):(r="gtm"+yc(),q=r+".");var w={name:!0,clientId:!0,sampleRate:!0,siteSpeedSampleRate:!0,alwaysSendReferrer:!0,allowAnchor:!0,allowLinker:!0,cookieName:!0,cookieDomain:!0,cookieExpires:!0,cookiePath:!0,cookieUpdate:!0,legacyCookieDomain:!0,legacyHistoryImport:!0,
storage:!0,useAmpClientId:!0,storeGac:!0},v={allowAnchor:!0,allowLinker:!0,alwaysSendReferrer:!0,anonymizeIp:!0,cookieUpdate:!0,exFatal:!0,forceSSL:!0,javaEnabled:!0,legacyHistoryImport:!0,nonInteraction:!0,useAmpClientId:!0,useBeacon:!0,storeGac:!0,allowAdFeatures:!0},u=function(a){var b=[].slice.call(arguments,0);b[0]=q+b[0];p.apply(window,b)},z=function(a,b){return void 0===b?b:a(b)},E=function(a,b){if(b)for(var c in b)b.hasOwnProperty(c)&&u("set",a+c,b[c])},B=function(){},G=function(a,b,c){var d=0;if(a)for(var e in a)if(a.hasOwnProperty(e)&&(c&&w[e]||!c&&void 0===w[e])){var g=v[e]?va(a[e]):a[e];"anonymizeIp"!=e||g||(g=void 0);b[e]=g;d++}return d},R={name:r};G(e,R,!0);p("create",b.vtp_trackingId||
c.trackingId,R);u("set","&gtm",Dg(!0));b.vtp_enableRecaptcha&&u("require","recaptcha","recaptcha.js");(function(a,c){void 0!==b[c]&&u("set",a,b[c])})("nonInteraction","vtp_nonInteraction");E("contentGroup",g);E("dimension",h);E("metric",k);var M={};G(e,M,!1)&&u("set",M);var F;b.vtp_enableLinkId&&u("require","linkid","linkid.js");
u("set","hitCallback",function(){var a=e&&e.hitCallback;na(a)&&a();b.vtp_gtmOnSuccess()});if("TRACK_EVENT"==b.vtp_trackType){}else if("TRACK_SOCIAL"==
b.vtp_trackType){}else if("TRACK_TRANSACTION"==b.vtp_trackType){}else if("TRACK_TIMING"==b.vtp_trackType){}else if("DECORATE_LINK"==b.vtp_trackType){}else if("DECORATE_FORM"==b.vtp_trackType){}else if("TRACK_DATA"==
b.vtp_trackType){}else{b.vtp_enableEcommerce&&(u("require","ec","ec.js"),B());if(b.vtp_doubleClick||"DISPLAY_FEATURES"==b.vtp_advertisingFeaturesType){var Aa="_dc_gtm_"+String(b.vtp_trackingId).replace(/[^A-Za-z0-9-]/g,"");u("require","displayfeatures",void 0,{cookieName:Aa})}if("DISPLAY_FEATURES_WITH_REMARKETING_LISTS"==b.vtp_advertisingFeaturesType){var ca=
"_dc_gtm_"+String(b.vtp_trackingId).replace(/[^A-Za-z0-9-]/g,"");u("require","adfeatures",{cookieName:ca})}F?u("send","pageview",F):u("send","pageview");}if(!a){var pa=b.vtp_useDebugVersion?"u/analytics_debug.js":"analytics.js";b.vtp_useInternalVersion&&!b.vtp_useDebugVersion&&
(pa="internal/"+pa);a=!0;T(O("https:","http:","//www.google-analytics.com/"+pa,e&&e.forceSSL),function(){var a=Dd();a&&a.loaded||b.vtp_gtmOnFailure();},b.vtp_gtmOnFailure)}}else C(b.vtp_gtmOnFailure)};Z.__ua=b;Z.__ua.g="ua";Z.__ua.h=!0;Z.__ua.b=0}();




Z.a.gas=["google"],function(){(function(a){Z.__gas=a;Z.__gas.g="gas";Z.__gas.h=!0;Z.__gas.b=0})(function(a){var b=t(a),c=b;c[P.qa]=null;c[P.ye]=null;var d=b=c;d.vtp_fieldsToSet=d.vtp_fieldsToSet||[];var e=d.vtp_cookieDomain;void 0!==e&&(d.vtp_fieldsToSet.push({fieldName:"cookieDomain",value:e}),delete d.vtp_cookieDomain);return b})}();
Z.a.smm=["google"],function(){(function(a){Z.__smm=a;Z.__smm.g="smm";Z.__smm.h=!0;Z.__smm.b=0})(function(a){var b=a.vtp_input,c=Y(a.vtp_map,"key","value")||{};return c.hasOwnProperty(b)?c[b]:a.vtp_defaultValue})}();



Z.a.paused=[],function(){(function(a){Z.__paused=a;Z.__paused.g="paused";Z.__paused.h=!0;Z.__paused.b=0})(function(a){C(a.vtp_gtmOnFailure)})}();
Z.a.html=["customScripts"],function(){function a(b,c,g,h){return function(){try{if(0<c.length){var d=c.shift(),e=a(b,c,g,h);if("SCRIPT"==String(d.nodeName).toUpperCase()&&"text/gtmscript"==d.type){var m=y.createElement("script");m.async=!1;m.type="text/javascript";m.id=d.id;m.text=d.text||d.textContent||d.innerHTML||"";d.charset&&(m.charset=d.charset);var p=d.getAttribute("data-gtmsrc");p&&(m.src=p,Oa(m,e));b.insertBefore(m,null);p||e()}else if(d.innerHTML&&0<=d.innerHTML.toLowerCase().indexOf("<script")){for(var q=
[];d.firstChild;)q.push(d.removeChild(d.firstChild));b.insertBefore(d,null);a(d,q,e,h)()}else b.insertBefore(d,null),e()}else g()}catch(r){C(h)}}}var c=function(d){if(y.body){var e=
d.vtp_gtmOnFailure,g=th(d.vtp_html,d.vtp_gtmOnSuccess,e),h=g.oc,k=g.I;if(d.vtp_useIframe){}else d.vtp_supportDocumentWrite?b(h,k,e):a(y.body,Xa(h),k,e)()}else eh(function(){c(d)},
200)};Z.__html=c;Z.__html.g="html";Z.__html.h=!0;Z.__html.b=0}();



var Ti={};Ti.macro=function(a){if(pf.$b.hasOwnProperty(a))return pf.$b[a]},Ti.onHtmlSuccess=pf.td(!0),Ti.onHtmlFailure=pf.td(!1);Ti.dataLayer=bd;Ti.callback=function(a){wc.hasOwnProperty(a)&&na(wc[a])&&wc[a]();delete wc[a]};Ti.Ve=function(){K[H.m]=Ti;xc=Z.a;Sb=Sb||pf;Tb=pd};
Ti.Jf=function(){K=x.google_tag_manager=x.google_tag_manager||{};if(K[H.m]){var a=K.zones;a&&a.unregisterChild(H.m)}else{for(var b=data.resource||{},c=b.macros||[],d=0;d<c.length;d++)Lb.push(c[d]);for(var e=b.tags||[],g=0;g<e.length;g++)Ob.push(e[g]);for(var h=b.predicates||[],k=0;k<h.length;k++)Nb.push(h[k]);for(var l=b.rules||[],m=0;m<l.length;m++){for(var p=l[m],q={},r=0;r<p.length;r++)q[p[r][0]]=
Array.prototype.slice.call(p[r],1);Mb.push(q)}Qb=Z;Rb=wh;Ti.Ve();Ne();sd=!1;td=0;if("interactive"==y.readyState&&!y.createEventObject||"complete"==y.readyState)vd();else{A(y,"DOMContentLoaded",vd);A(y,"readystatechange",vd);if(y.createEventObject&&y.documentElement.doScroll){var w=!0;try{w=!x.frameElement}catch(E){}w&&wd()}A(x,"load",vd)}Be=!1;"complete"===y.readyState?De():A(x,"load",De);a:{if(!Sc)break a;x.setInterval(Tc,864E5);}tc=(new Date).getTime();}};(0,Ti.Jf)();

})()
