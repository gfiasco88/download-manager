import pickle
import threading
import traceback
from downloader import UniversalDownloader
import config
import logging
import os


class MyQueue:
    '''
    FIFO QUEUE with save and load from file capabilities
    order of operation:
    - Try to load existing queue from `savefile` 
    - if empty or not present, then creates a new one
    '''
    def __init__(self,q=[],savefile='myqueue.obj'):
        self.dump=savefile
        try:
            self.load()
        except:        
            self.q=q

    def __repr__(self):
        return 'MyQueue(q={},savefile=\'{}\')'.format(repr(self.q),self.dump)

    def enqueue(self,item):
        self.q.append(item)
        self.save()
        return True
    
    def dequeue(self):
        if self.size()==0:
            return
        return self.q.pop(0)

    def size(self):
        return len(self.q)

    def save(self):
        with open(self.dump,'wb+') as f:
            pickle.dump(self.q,f)

    def load(self):
        with open(self.dump,'rb+') as f:
            self.q=pickle.load(f)

    def isEmpty(self):
        return len(self.q)==0

    def items(self):
        return (list(enumerate(self.q)))

class Workers:
    '''
        Download workers:
            - N number of Threads picking url from a list of urls
    '''
    def __init__(self,logger,num_threads=2,q=MyQueue()):
        
        self.q=q
        self.logger=logger
        self.workers={}
        for i in range(num_threads):
            worker = threading.Thread(target=self.Download)
            worker.setDaemon(True)
            worker.start()
            self.workers[worker.name]=None

    def Download(self):
        ''' pickup url from the queue and download it, then save the queue'''
        while True:
            url=self.q.dequeue()
            if url:
                tid=threading.current_thread().name 
                self.logger.info("{} - downloading file {}".format(tid,url))
                try:
                    self.workers[tid]=UniversalDownloader(url)
                    self.workers[tid].download(
                        oPath=config.downloadPath,
                        dPath=config.postProcessPath,
                        filters=config.ytfilters,
                        fmApiKey=config.fmApiKey,
                        )
                    self.q.save()
                except AttributeError:
                    continue
                except Exception as e:
                    self.logger.error(traceback.format_exc())
                    self.workers[tid].dl.progress="Failed"

    def progress(self):
        report={}
        for w in self.workers.keys():
            obj=self.workers[w]
            if obj:
                try:
                    percent=int(self.workers[w].status())
                except ValueError:
                    percent=self.workers[w].status()
                title=self.workers[w].dl.title
                self.logger.info("{} - progress {}% of {}".format(w,percent,title))
                report[w]={'file_title':title,"percent":percent}
        return report


if __name__=='__main__':

    print ("start testing the queue class, queueing data")
    q=MyQueue()
    for i in [1,2,3,'b']:
        q.enqueue(i)
    q.save()
    print ("Q saved on ", q.dump)

    z=MyQueue()
    z.load()
    print ("Loaded queue of size", z.size())
    if z.dequeue() == q.dequeue():
        print ("queues match, dump was succesful")
