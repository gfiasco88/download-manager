#!/usr/bin/env python3

import pytube
#import config
try:
    from urlparse import urlparse
except ModuleNotFoundError:
    from urllib.parse import urlparse
import spotipy
import os
import ffmpeg
from mp3util import Mp3

import logging
logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

class UniversalDownloader:

    '''
        Download using the service based on url
        interface to classes
    '''

    def __init__(self, url):
        self.url=url
        self.dl=self.__service()

    def status(self):
        return self.dl.progress

    def download(self,**kwargs):
        self.dl.download(**kwargs)

    def __service(self):
        domain=urlparse(self.url)
        if domain.netloc in ('youtu.be','www.youtube.com','youtube.com'):
            logger.info("YouTube Link detected")
            return YtDl(self.url)
        elif domain.netloc in ('open.spotify.com','spotify.com'):
            logger.info("spotify link detected")
            return('working in progress')
        else:
            raise ValueError('URL/Service {} not supported'.format(self.url))



class YtDl:
    def __init__(self,url):
        self.video=pytube.YouTube(url,on_progress_callback=self.progressCheck)
        self.title=self.video.title
        self.fileSize=0
        self.progress=0

    def __repr__(self):
        return "YtDL(url={})".format(self.video)

    def download(self,**kwargs):
        mp4=self.video.streams.filter(**kwargs['filters']).first()
        self.fileSize=mp4.filesize
        logger.info("processing file {} of size {}".format(mp4.default_filename,self.fileSize))
        mp4.download(kwargs['oPath'])
        if kwargs['filters']['only_audio'] == True:
            self.progress="Converting to Mp3..."
            input_file="{}/{}".format(kwargs['oPath'],mp4.default_filename)
            output_file="{}/{}.mp3".format(kwargs['dPath'],self.title)
            mp3=Mp3(input_file,kwargs['fmApiKey']).run(output_file)       
            self.progress="Completed. {}".format(mp3)
            return True

    def progressCheck(self,stream = None, chunk = None, file_handle = None, remaining = None):
        '''
            Gets the percentage of the file that has been downloaded.
        '''
        percent = (100*(self.fileSize-remaining))/self.fileSize
        self.progress=percent



class SpotDl:
    
    def __init__(self,url,token):
        self.sp=spotipy.Spotify(token)
        self.t=self.sp.track(url)
        self.meta=self.__songMetaData()
        

    def download(self,**kwargs):
        pass

    def __songMetaData(self):
        meta['title']=self.t['name']
        meta['album']=self.t['album']['name']
        meta['artists']=self.t['artists'][0]['name']
        return meta


if __name__=='__main__':
    from os import system
    import sys

    print ("downloading test file in /tmp")

    url = 'https://www.youtube.com/watch?v=kXYiU_JCYtU'
    
    DNW=UniversalDownloader(url)

    DNW.download(oPath='/tmp',dPath='/music',filters={'only_audio':True,},fmApiKey='f4aa1cac32f135ec8257894e4108ce33')

    print ('completed')
