#!/usr/bin/env python3
from flask import Flask,redirect,request,render_template,url_for, abort
from flask import jsonify,session,flash, Response
from queuemng import MyQueue, Workers
from functools import wraps
import secrets
import config
import logging
from datetime import datetime

# GLOBAL VARIABLES
LOGFILE='WSGI.log'
q=MyQueue()

application = Flask(__name__,static_url_path='/static')
W=Workers(logger=application.logger,num_threads=config.numWorkers,q=q)


@application.before_first_request
def setup_logging():
    if not application.debug:
        # In production mode, add log handler to sys.stderr.
        #application.logger.addHandler()
        application.logger.setLevel(logging.INFO)

def check_auth(username, password):
    """This function is called to check if a username /
    password combination is valid.
    """
    return username == config.adminUser and password == config.adminPass
        
# def authenticate():
#     """Sends a 401 response that enables basic auth"""
#     return Response(
#     'Could not verify your access level for that URL.\n'
#     'You have to login with proper credentials', 401,
#     {'WWW-Authenticate': 'Basic realm="Login Required"'})

def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if not session.get('logged_in'):
            auth = request.authorization
            if not auth or not check_auth(auth.username, auth.password):
                return render_template('login.html')
        return f(*args, **kwargs)
    return decorated

@application.errorhandler(404)
def page_not_found():
    return render_template('404.html'), 404

@application.errorhandler(500)
def internal_error(error):
    return "500 Application ERROR:{}".format(error)

@application.route('/favicon.ico')
def favicon():
    return application.send_static_file('favicon.ico')

@application.route('/')
@requires_auth
def home():
    return application.send_static_file('index.html')

@application.route('/login', methods=['POST'])
def do_admin_login():
    if request.form['password'] == config.adminPass and request.form['username'] == config.adminUser:
        session['logged_in'] = True
        application.logger.info('%s logged in successfully',request.form['username'])
    else:
        flash('wrong password!')
    return home()

@application.route('/d/<path:path>')
def ignore(path):
    return 'You want path: %s' % path

@application.route('/download')
@requires_auth
def addLinkToList():
    url = request.args.get('url')
    if url:
        q.enqueue(url)
        return redirect(url_for('stats'))
    return 'Error: Not Valid Url',501

@application.route('/stats')
@requires_auth
def stats():
    report=W.progress()
    todo=q.items()
    json=request.args.get('json')
    if json:
        report['queue']=todo
        return jsonify(report)
    return render_template('stats.html',result=report,queue=todo,time=str(datetime.now()))
    
if __name__ == "__main__":
    print (__name__)
    application.secret_key = secrets.token_bytes(12)
    application.run(host='0.0.0.0',port=8080,threaded=True)