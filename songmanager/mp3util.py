
import ffmpeg
import os
import requests
import json
import re
import pylast
from mutagen.easyid3 import EasyID3
import logging
logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)
import traceback


__author__='Gian Luca Fiasco'
# https://www.last.fm/api
# https://github.com/pylast/pylast/tree/master/pylast
# https://mutagen.readthedocs.io/en/latest/user/id3.html



class Mp3:

    '''
        Utility to Convert and Tag using ID3v1 standard files
        - convert uses ffmpeg to convert to mp3
        - music.fm to gather info and tag
            tags: title, artist, album, year
    '''

    def __init__(self,mp3file,api_key):
        self.mp3={
            'filename':mp3file,
            'title': None,
            'artist': None,
            'album': None,
            'date': None
        }
        self.lastFm=pylast.LastFMNetwork(api_key=api_key)

        #self.apiUrl='http://ws.audioscrobbler.com/2.0/'

    def run(self,output_file):
        # import logging.config
        self.genTag()
        self.convert(output_file)
        self.applyTag(output_file)
        return self.checkTags(output_file)


    def __str__(self):
        return str(self.mp3)

    def genTag(self):
        ''' assuming first result is the best match'''
        track=self.__searchTracks()[0]
        self.mp3['title']=track.get_title()
        self.mp3['artist']=track.get_artist().get_name()
        self.mp3['albumartist']=track.get_artist().get_name()
        if track.get_album():
            self.mp3['album']=track.get_album().get_title()
            self.mp3['date']=track.get_wiki('published').split(',')[0]
        logger.info("Mp3 Tag retrived")
        return

    def convert(self,output_file):
        if not output_file:
            output_file=self.mp3['filename'].rstrip('.mp4')+'.mp3'
        logger.info('[ffmpeg] conversion started')
        ffmpeg.input(self.mp3['filename']).output(output_file).overwrite_output().run()
        logger.info("[ffmpeg] converted file {}".format(output_file))
        #os.remove(self.mp3['filename'])
        self.mp3['filename']=output_file
        return
        

    def applyTag(self,output_file,tags=None):
        if not tags:
            tags=self.mp3
        audio = EasyID3(output_file)
        for tag in tags.keys():
            if tag == 'filename':
                continue
            try:
                audio[tag] = tags[tag]
            except:
                logger.error("mp3 apply tag={} failed ".format(tag))
                logger.debug(traceback.format_exc())
                pass
        audio.save()
        logger.info("mp3 tags {} applied to {}".format(self.checkTags(output_file),output_file))
        return 

    def checkTags(self,mp3file):
        return str(EasyID3(mp3file))

    def __searchTracks(self,artist=''):
        searchString=self.renamer()
        logger.info("Searching info of {} via lastFM".format(searchString))
        tracks=self.lastFm.search_for_track(track_name=searchString,artist_name=artist)
        return tracks.get_next_page()
     
    def renamer(self):
        '''remove set of patterns from filename:
            - lyrics
            - HD
            - (* [Vv]ideo)
        '''
        rename=os.path.split(self.mp3['filename'])[-1].rstrip('.mp4').replace('[','(').replace(']',')')

        terms=(
            'Lyrics',
            '(Music Video)',
            '(Official Video)',
            '(Official Music Video)',
            ' HD ',
            '(with )',
            '(  )',
            '[ HQ ]'
        )
        for t in terms:
            rename=rename.replace(t.capitalize(),'')
            rename=rename.replace(t.title(),'')
            rename=rename.replace(t.upper(),'')
            rename=rename.replace(t.lower(),'')

        logger.info('Applied regex : {}'.format(rename))
        return rename.rstrip(' ')


if __name__=='__main__':
    # some tests
    import config
    s="/download/Evanescence - Bring Me To Life (Official Music Video).mp4"
    print("filename: \n",s)
    M=Mp3(s,config.fmApiKey)
    print(M.run('/download/test.mp3'))
    #m2.run()



    