FROM python:3
MAINTAINER Gian Luca Fiasco <glf@lucacloud.info>

# Install extra packages
RUN apt-get update
RUN apt-get install ffmpeg -y

RUN mkdir /usr/src/app

WORKDIR /usr/src/app
ADD requirements.txt /usr/src/app
ADD ./songmanager/ /usr/src/app

RUN pip install -r requirements.txt
EXPOSE 8080
VOLUME [ "/download" ] 

ENTRYPOINT [ "python3","wsgi.py" ]


## CLEANUP

RUN apt-get autoremove -y
RUN apt-get clean && rm -rf /var/lib/apt/lists/*