# Download Manager

## "Rest" server to Rip music form the web

Web server routes:
- /download?uri - accept youtube links (spotify in the future???)
- /stats - show current "ready" queue and workers download status
- /login - support "session" authentication and basic auth

## Install

too easy, just use docker

```bash
docker run -d -p 8080:8080 -v <machine_download_path>:/download -e flask_user='user' -e flask_passwd='foobar' dockrepo.lucacloud.info/fiasco/download-manager:latest
```

### Variables

- flask_user : username authorized to download (default 'admin')
- flask_passwd : its password (default 'password')
- num_workers : number of max parallels download (2 by default)

## Screenshots

1. **login**

![](https://lh3.googleusercontent.com/lAGl5VODrIHt6F0lHmrPpygiawb5G7W3Vpdq18Zvnb_f1AcM5jxJfl7iFv3M6iYiXkiJddmAAKyw0rFCe-2EAqFZ4en2CS729dV86VEAmnUbeXabLidibTXL6aqt776XkGEQgLENEmJ6CL-BUfYSp39CBD7x6-7PCxwSVfVZdGTy9pyx4j9WDA969FsXufVVPyJCLxav6IGkJz9Zq0TUlgCqrSAF4W24gwwU-m0hb-PYln8IixTHo9u2MKWptZOZkYizCmR6PrQtF31ZBslXGTiaS4fih6hzk8ldmX4E5lTLXtz30s949hIJAjVuzgA-qlHYNuMjgAyqpE-M3u1maKbUfq6-Ssx-HgE5bGuH2G_HM0B0Snc8udBWHtjmlJ9L2bjVHkNNqeYkGdk68bpJZPDdp87_X5Pahh0o190u7yCqjwarSN-7hhYHJc_XCj4BGBeNiGASQeQVkk2Yc8yQBrf8awIQxgmLwioD5x3B5WOjBV2MyH7ZqOH3hGA8S50f6OjmCGkGogy86nMuHZ4mL8kMBwHsKFek3PNyvI8bwMO0E-bDvnXrHH9s0wK8ULVShifoas9j-H6l1s5Y0jsB7jXaDL4VbPiB32cN7Wbd6IBY8L8irLZkSfAvXJLA8Mmoz4RLLGhu1eV3D1veaayaMVudOV5BUQt3iD_csTIPf_FS2v5f7GUbTBTPVMVTPcitCYIu4E9JYpY3Sw5KETKfuYbdlw=w1460-h1306-no)

2. **home**

![](https://lh3.googleusercontent.com/fT6L8LxNM4dDK4VCfL6_G8mzPHwf59MeyhBzAwyCEm-3DPK1M-BBllUqx_nL763JBPTvGsUv4lQFnR1WoISG045R7OuPXI66B6FaxcpLhm29X7xeYbyMrppSdHqIKQHlUKh3fAhSPLU3D45q2N2CdUvOTjncFr7NxXzQC2bKNpo9jkCas6-hyGTV8bkD_7op-M5PCFWO1H5liUuv0_nxgEZ80y-1fYC5Hqqzb9Wa_ONNOYjtYoTI6Nh3vjWKs1JuC04cSsi1SQqZuroRmlw4sDqwZrbzLLqZ7zA8hpDN7T4f8KmOQAxvPupuMdPAHyjaZBBUEq3I-A4kyMwSlIe624T2wSgiAIocWtSpVrSXWKd2usan9GoNe_4d5EtJjxd9VaJXrJL7wYvyZzg9d7QmD6vcpc38IHMhZE8OqYK8F_GK3MzXngCPd7RO8PgIHLc2gKA-a73e3oDILVxhhegAeoM2qRskz_lSVr_GmZ6WRhXFUwfybQ1rS92_DI4gSKFHXKRXM_MFqIeCwtk711OAXA8P6qNg8jjj9d0FqN7pdWS4Nq-KuyC6NazU9rGCkq-E97EhisF_FjonMxGS4qUt__GYirrskb4D-b4hYtO7gg9y6B67BbXQsKJXRsJXob76YQgsdFSAOCVR8Eq4Mf4WPx22CuKnWc72euVhld1GKYh9xSBaXpULcXTswXIkH-3B8SOk9-hfXkr_Nk51k--_RXOtlQ=w1990-h1358-no)

3. **stats**
![](https://lh3.googleusercontent.com/KFGY3whxrDAHPSavWoLfV1BxoGkCvVnYFOjfWl7gsO6epcaD7iBCd2bYaLLm1JGzhEiCPzeRfTGrgduMXjhl_3GGLnDI_HeJfyEKqPQetFvmyK6S40KJF74cTtZ-meE0v7PWduG6_c168dXRTjadhE7aUyklxRQ0eS7jCx3-_XqTjdYsmLRacEYkCTlCzocmKWvLLQxhct9B9ruOwxydmsofZALGZJqVWxabiFSAPEp0fJfQ0vid_HEoWl7vgHePVYudqNvtJTfXHffeAF6q1zs3dF21cLv5P04ePpICiGdrgRzvb7CehABJNASlGWKazUQ7BqRtHauLZ2NNUoIrETgPyBkz8YP0jr0qhk92iRDHlArzK2vQ_HtQNPTZ-6uWXZvVbxd2EhK1c-Z5rhp8ZfIOa3ZaEKKm9k2Gb1l0W1Q_wnNvjSN_08sS8FMw57CKJ7M2uKuGaDXJYpi_7sqwid4W3rjfZ4qacx9_IbtV_ctm5WyOEICeU0iDoFpDMgseXzYztRygDzNP2bfJE7Jvmwo4jdX8Cz9qaLKIcXcd9lL-GBpli6kgtWHcBvZuLrQFaOCkpGW47uhapqaXpViO9Esfjp1kNczO1gFq-uVfLAGAu2WIEOPxNnxF41ncR30H0S8RC-F6red5_Ud-vEVbY-A1nCX2u8XGPbfmT8RK2NUwuMc8Ggs90aBhywoLy4poQ1gPSBTE2QMeeSKW51rSb0jmbQ=w1586-h1116-no)

### Disclaimers
- Not responsible for your copyright violations, this is a fun project nothing more and nothing less.
- Things can always be made more efficient, so please no mock me down especially when I have limited time for personal projects.
- no experience whatsoever with Android



### References

1. [https://python-pytube.readthedocs.io/en/latest/] (https://python-pytube.readthedocs.io/en/latest/)
2. [https://pypi.org/project/spotify-dl/](https://pypi.org/project/spotify-dl/)